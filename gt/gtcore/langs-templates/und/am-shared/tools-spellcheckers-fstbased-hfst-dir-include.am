## Process this file with automake to produce Makefile.in

## Copyright (C) 2011 Samediggi

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

GT_SPELLER_HFST=generator-fstspeller-gt-norm.hfst
GT_SPELLER_ACCEPTOR=acceptor.default.hfst

# This is how we zip files to work with hfst-ospell and libvoikko
# (=max compression, working from 28.1.2014 for all platforms):
ZIPFLAGS=-9

# Let the verbosity of some command line tools follow the automake verbosity:
VERBOSITY=$(if $(strip $(filter-out false,$(AM_V_P))), ,-q)

####### Automake targets: ########

GT_ERRMODELS=
if WANT_SPELLERS
if CAN_HFST
#GT_ERRMODELS+=errmodel.edit-distance-1.hfst

# Only build speller if it is enabled (default=yes)
if WANT_SPELLERAUTOMAT
GT_ERRMODELS+=errmodel.default.hfst
# libvoikko can't yet handle multiple error models - do NOT include this line:
# errmodel.ocr.hfst

# Require zip to turn on zhfst target:
if WANT_VOIKKO
GT_SPELLING_HFST=$(GTLANG2).zhfst
endif # WANT_VOIKKO
endif # WANT_SPELLERAUTOMAT
endif # CAN_HFST

if CAN_HFST
if WANT_VOIKKO
voikkosharedir=$(libdir)/voikko/3/
#! @param GT_VOIKKO optional, set to spell checker automata names if
#!					installable
voikkoshare_DATA=$(GT_SPELLING_HFST)
endif # WANT_VOIKKO
endif # CAN_HFST
endif # WANT_SPELLERS

noinst_DATA=$(GT_ERRMODELS) $(OXT_FILES)

###################################
####### HFST build rules: #########

####### Esater egg version info: #######
# Easter egg content:
easteregg.txt: $(srcdir)/version.txt
	$(AM_V_GEN)$(GTCORE)/scripts/make-hfstspeller-version-easter-egg.sh \
		$(GTLANG2) $(top_srcdir) $^ > $@

# Easter egg suggestions:
easteregg.suggs.txt: easteregg.txt
	$(AM_V_GEN)sed -e 's/^/nuvviDspeller:/' < $< \
		| sed = \
		| sed 'N;s/\n/	/' \
		| perl -pe 's/(.)\t(.+)/\2\t0.0\1/' \
		> $@

# Easter egg string acceptor:
easteregg.hfst: easteregg.txt
	$(AM_V_GEN)$(HFST_STRINGS2FST) $(HFST_FLAGS) -j < $< \
		| $(HFST_PROJECT) $(HFST_FLAGS) --project=lower > $@

####### Error model: #######
# Error model building - edit distance based on transducer alphabet:
editdist.%.hfst: editdist.%.txt $(GT_SPELLER_ACCEPTOR)
	$(AM_V_GEN)$(GTCORE)/scripts/editdist.py -v -s -d 1 -e '@0@' -i $<   \
		-a $(GT_SPELLER_ACCEPTOR) \
		| $(HFST_TXT2FST) $(HFST_FLAGS) -e '@0@' -o $@

# Helper fst:
anystar.hfst:
	$(AM_V_GEN)echo "?*;" | $(HFST_REGEXP2FST) -S -o $@

# Error model building - list of strings known to be misspelled:
strings.%.hfst: strings.%.txt anystar.hfst
	$(AM_V_GEN)grep -v '^#' $< | grep -v '^$$'   \
		| $(HFST_STRINGS2FST) $(HFST_FLAGS) -j \
		| $(HFST_CONCATENATE)   anystar.hfst - \
		| $(HFST_CONCATENATE) - anystar.hfst   \
		-o $@

# Combine edit distance with string pattern edits:
editStrings.%.hfst: strings.%.hfst editdist.%.hfst
	$(AM_V_GEN)$(HFST_DISJUNCT) $^       \
		| $(HFST_MINIMIZE) $(HFST_FLAGS) \
		| $(HFST_REPEAT) -f 1 -t 2       \
		-o $@

# Error model building - list of words known to be misspelled:
words.%.hfst: words.%.txt easteregg.suggs.txt
	$(AM_V_GEN)grep -h -v '^#' $^ | grep -v '^$$'   \
		| $(HFST_STRINGS2FST) $(HFST_FLAGS) -j -o $@

# The final error model is assembled here:
errmodel.%.hfst: words.%.hfst editStrings.%.hfst
	$(AM_V_GEN)$(HFST_DISJUNCT) $^ \
		| $(HFST_FST2FST) $(HFST_FLAGS) -f olw -o $@

####### Alternate error model: #######
# Alternatively, the error model can be constructed as a long list of regular
# expressions, semicolon separated:
errmodel.%.hfst: errmodel.%.regex easteregg.suggs.hfst
	$(AM_V_GEN)$(HFST_REGEXP2FST) $(HFSTFLAGS) -S -i $< \
		| $(HFST_DISJUNCT) - easteregg.hfst \
		-o $@

####### Add corpus-based weights: #######
# copy cleaned corpus into the local dir:
$(CORPUSNAME).clean.txt: \
	$(top_srcdir)/tools/spellcheckers/fstbased/data/$(CORPUSNAME).clean.txt
	$(AM_V_GEN)cp -f $< $@

# sort the clean corpus:
%.sort.txt: %.clean.txt
	$(AM_V_GEN)sort < $< > $@

# token count:
%.wordcount.txt: %.sort.txt
	$(AM_V_GEN)wc -l < $< > $@

%.uniq.txt: %.sort.txt
	$(AM_V_at)uniq -c < $< > $@

# type count:
%.typecount.txt: %.uniq.txt
	$(AM_V_GEN)wc -l < $< > $@

# calculate unit weight, smoothed using ALPHA:
%.unitweight.txt: %.wordcount.txt %.typecount.txt
	$(AM_V_GEN)paste $^ |\
		sed -e "s/^/scale=5; -l($(ALPHA)\/(/" \
		-e "s/	/ + ($(ALPHA) */" -e "s/$$/)))/" |\
		bc -l > $@

# add tropical weights to the corpus:
%.tropical.txt: %.uniq.txt %.wordcount.txt %.typecount.txt
	$(AM_V_GEN)cat $< |\
		$(GAWK) -v CS="$$(cat $*.wordcount.txt)" -v ALPHA=$(ALPHA) \
		-v DS="$$(cat $*.typecount.txt)" -f $(srcdir)/uniqc2tropical.awk > $@

# build an fst of surface forms with tropical weights for each word form:
%.surfs.hfst: %.tropical.txt
	$(AM_V_GEN)cat $< |\
		$(HFST_STRINGS2FST) -j $(HFST_FLAGS) -f openfst-tropical -o $@

# Keep these intermediate targets when building using --debug:
.SECONDARY: spellercorpus.sort.txt \
            spellercorpus.surfs.hfst \
            spellercorpus.tropical.txt \
            spellercorpus.typecount.txt \
            spellercorpus.wordcount.txt \
            word-boundary.hfst

####### Speller acceptor: #######
# Build the automaton used for the speller
$(GT_SPELLER_ACCEPTOR): $(GT_SPELLER_HFST) easteregg.hfst \
						$(top_builddir)/src/filters/remove-word-boundary.hfst
	$(AM_V_GEN)cat $< \
		| $(HFST_COMPOSE) $(HFST_FLAGS) -F \
			-2 $(top_builddir)/src/filters/remove-word-boundary.hfst \
		| $(HFST_PROJECT) $(HFST_FLAGS) --project=lower \
		| $(HFST_MINIMIZE_SPELLER) $(HFST_FLAGS)        \
		| $(HFST_DISJUNCT) - easteregg.hfst             \
		| $(HFST_FST2FST) $(HFST_FLAGS) -f olw          \
		-o $@

####### *.zhfst file: #######
# Finally build the zhfst file, and make a copy in a subdir named '3', so that
# we can test it without installing it (the '3' dir is a voikko requirement):
$(GT_SPELLING_HFST): index.xml \
					 $(GT_ERRMODELS) \
					 $(GT_SPELLER_ACCEPTOR)
	$(AM_V_GEN)rm -f $@ && $(ZIP) $(ZIPFLAGS) -j $@ $^ && \
		$(MKDIR_P) 3 && \
		cp -f $@ 3/

####### *.oxt files: #######
# Hard-coded paths for the time being - to be generalised later.

OXT_FILES=

if WANT_SPELLERS
if CAN_HFST
if CAN_LOCALSYNC

# Generate target file names dynamically based on the variables VK_VERSIONS
# and VK_PLATFORMS, whose cross product is available in VOIKKO_VERS_PLATFORMS.
# The resulting file names follow this pattern:
#
#$(GTLANG2)_LO-voikko-3.4.1-win.oxt
OXT_FILES+=$(addsuffix .oxt, \
              $(addprefix $(GTLANG2)_LO-voikko-,$(VOIKKO_VERS_PLATFORMS)))


endif # CAN_LOCALSYNC
endif # CAN_HFST
endif # WANT_SPELLERS

#### Voikko versions: ####
# 3.4.1
# 4.0
#
# These values corresponds to the version ID in the dir name of
# the oxt template
VK_VERSIONS=3.4.1 4.0

# LO-Voikko platforms:
VK_PLATFORMS=mac win

VOIKKO_VERS_PLATFORMS=$(foreach version,$(VK_VERSIONS), \
                    $(addprefix $(version)-,$(VK_PLATFORMS)))

OXT_ROOT_DIR=$(GTHOME)/prooftools/toollibs/LibreOffice-voikko

# Build receipt for OXT:
# mkdir build dir
# rsync into build/subdir
# cp zhfst
# modify oxt index file
# zip build/subdir
# copy to ./

$(GTLANG2)_LO-voikko-%.oxt: $(GT_SPELLING_HFST)
	$(AM_V_GEN)rm -f $@ && \
		if [ -d "$(OXT_ROOT_DIR)/$*" ]; then \
			$(MKDIR_P) build/$*/ && \
			cd build/$* && \
			$(RSYNC) -av $(VERBOSITY) $(OXT_ROOT_DIR)/$*/ ./ && \
			cp ../../$< 3/ && \
			$(ZIP) -r $(VERBOSITY) $(ZIPFLAGS) ../../$@ * ; \
		else \
			echo "  SKIP     $@: Not yet supported." ; \
		fi

DATE=$(shell date +%Y%m%d)
UPLOADSITE=sd@divvun.no:static_files/oxts

upload: $(OXT_FILES)
	$(AM_V_GEN)for file in $(OXT_FILES); do \
		if [ -e $$file ]; then \
			stem=$$(basename $$file .oxt) ; \
			scp $$file $(UPLOADSITE)/$$stem-$(DATE).oxt ; \
			ssh sd@divvun.no "cd staticfiles/oxts/ && \
			ln -sf $$stem-$(DATE).oxt $$file" ; \
			echo "  SYMLINK  $$file" ; \
		fi \
	done

####### Other targets: ###########
clean-local:
	-rm -rf *.hfst *.xfst *.zhfst 3 easteregg.* $(CORPUSNAME).*

# vim: set ft=automake:
