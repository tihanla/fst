#!/bin/bash
# @configure_input@

# Automake interprets the exit status as follows:
# - an exit status of 0 will denote a success
# - an exit status of 77 a skipped test
# - an exit status of 99 a hard error
# - any other exit status will denote a failure.


# Test that all noun lemmas do generate as themselves:


###### Variables: #######
sourcefile=${srcdir}/../../../src/morphology/stems/nouns.lexc
lemmas=./nouns.txt
generatedlemmas=./gen-nouns
failedlemmas=./fail-lemmas
generatorfile=./../../../src/generator-gt-norm
resultfile=lemma-gen-diff

# FAIL if source file does not exist:
if [ ! -f $sourcefile ]; then
    echo
    echo "*** Warning: Source file $sourcefile not found."
    echo
    exit 77
fi

# Use autotools mechanisms to only run the configured fst types in the tests:
fsttype=
@CAN_HFST_TRUE@fsttype="$fsttype hfst"
@CAN_XFST_TRUE@fsttype="$fsttype xfst"

# Exit if both hfst and xerox have been shut off:
if test -z "$fsttype" ; then
    echo "All transducer types have been shut off at configure time."
    echo "Nothing to test. Skipping."
    exit 77
fi

# Get external Mac editor for viewing failed results from configure:
EXTEDITOR=@SEE@

###### Extraction: #######
@GTCORE@/scripts/extract-lemmas.sh \
	$sourcefile "(CmpN/Last)" > $lemmas

###### Start testing: #######
transducer_found=0
Fail=0

# The script tests both Xerox and Hfst transducers if available:
for f in $fsttype; do
	if [ $f == "xfst" ]; then
		lookuptool="@LOOKUP@ -q -flags mbTT"
		suffix="xfst"
	elif [ $f == "hfst" ]; then
		lookuptool="@HFST_LOOKUP@ -q"
		suffix="hfstol"
	else
	    Fail=1
		printf "ERROR: Unknown fst type! "
	    echo "$f - FAIL"
	    continue
	fi
	if [ -f "$generatorfile.$suffix" ]; then
		let "transducer_found += 1"

###### Test non-comopunds: #######
		# generate nouns in Singular, extract the resulting generated lemma,
		# store it:
		sed 's/$/+N+Sg+Nom/' $lemmas | $lookuptool $generatorfile.$suffix \
			| cut -f2 | fgrep -v "+N+Sg" | grep -v "^$" | sort -u \
			> $generatedlemmas.$f.txt 
#		# Generate nouns, extract those that do not generate in singular,
#		# generate the rest in plural:
#		sed 's/$/+N+Sg+Nom/' $lemmas | $lookuptool $generatorfile.$suffix \
#			| cut -f2 | grep "N+" | cut -d "+" -f1 | sed 's/$/+N+Pl+Nom/' \
#			| $lookuptool $generatorfile.$suffix | cut -f2 \
#			| grep -v "^$" >> $generatedlemmas.$f.txt 

###### Collect results: #######
		# Sort and compare original input with resulting output - the diff is
		# stored and opened in SEE:
		sort -u -o $generatedlemmas.$f.txt $generatedlemmas.$f.txt 
		comm -23 $lemmas $generatedlemmas.$f.txt > $resultfile.$f.txt

		# if at least one word is found, the test failed, and the list of failed
		# lemmas is opened in SubEthaEdit:
		if [ -s $resultfile.$f.txt ]; then
			# Only open the failed lemmas in see if @SEE@ is defined:
			if [ "$EXTEDITOR" ]; then
				$EXTEDITOR $resultfile.$f.txt
			fi
		    Fail=1
		    echo "$f - FAIL"
		    continue
		fi
	    echo "$f - PASS"
	fi
done

# When done, remove the generated data file:
rm -f $lemmas

# At least one of the Xerox or HFST tests failed:
if [ "$Fail" = "1" ]; then
	exit 1
fi

if [ $transducer_found -eq 0 ]; then
    echo ERROR: No transducer found
    exit 77
fi
