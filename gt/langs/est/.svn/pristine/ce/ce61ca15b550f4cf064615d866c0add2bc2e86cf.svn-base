! Divvun & Giellatekno - open source grammars for Sámi and other languages
! Copyright © 2000-2010 The University of Tromsø & the Norwegian Sámi Parliament
! http://giellatekno.uit.no & http://divvun.no
!
! This program is free software; you can redistribute and/or modify
! this file under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version. The GNU General Public License
! is found at http://www.gnu.org/licenses/gpl.html. It is
! also available in the file $GTHOME/LICENSE.txt.
!
! Other licensing options are available upon request, please contact
! giellatekno@hum.uit.no or divvun@samediggi.no

! ==================== !
! The Estonian clock !
! ==================== !

! Adapted to Estonian by Heli Uibo

Multichar_Symbols

@U.TIME.1@ 
@U.TIME.2@ 
@U.TIME.3@ 
@U.TIME.4@ 
@U.TIME.5@ 
@U.TIME.6@ 
@U.TIME.7@ 
@U.TIME.8@ 
@U.TIME.9@ 
@U.TIME.10@ 
@U.TIME.11@ 
@U.TIME.12@ 

+Use/NG
+Use/NA

LEXICON Root

! Allowing sloppy 2:30 in answer
1@U.TIME.1@+Use/NA:@U.TIME.1@     AFTpunkt ;    !01:00 - 01:19
1@U.TIME.2@+Use/NA:@U.TIME.2@     BEFpunkt ;    !01:20 - 01:59
2@U.TIME.2@+Use/NA:@U.TIME.2@     AFTpunkt ;    !02:00 - 02:19
2@U.TIME.3@+Use/NA:@U.TIME.3@     BEFpunkt ;    !02:20 - 02:59
3@U.TIME.3@+Use/NA:@U.TIME.3@     AFTpunkt ;    !03:00 - 03:19
3@U.TIME.4@+Use/NA:@U.TIME.4@     BEFpunkt ;    !03:20 - 03:59
4@U.TIME.4@+Use/NA:@U.TIME.4@     AFTpunkt ;    !04:00 - 04:19
4@U.TIME.5@+Use/NA:@U.TIME.5@     BEFpunkt ;    !04:20 - 04:59
5@U.TIME.5@+Use/NA:@U.TIME.5@     AFTpunkt ;    !05:00 - 05:19
5@U.TIME.6@+Use/NA:@U.TIME.6@     BEFpunkt ;    !05:20 - 05:59
6@U.TIME.6@+Use/NA:@U.TIME.6@     AFTpunkt ;    !06:00 - 06:19
6@U.TIME.7@+Use/NA:@U.TIME.7@     BEFpunkt ;    !06:20 - 06:59
7@U.TIME.7@+Use/NA:@U.TIME.7@     AFTpunkt ;    !07:00 - 07:19
7@U.TIME.8@+Use/NA:@U.TIME.8@     BEFpunkt ;    !07:20 - 07:59
8@U.TIME.8@+Use/NA:@U.TIME.8@     AFTpunkt ;    !08:00 - 08:19
8@U.TIME.9@+Use/NA:@U.TIME.9@     BEFpunkt ;    !08:20 - 08:59
9@U.TIME.9@+Use/NA:@U.TIME.9@     AFTpunkt ;    !09:00 - 09:19
9@U.TIME.10@+Use/NA:@U.TIME.10@   BEFpunkt ;    !09:20 - 09:59

! Correct is 02:30
%01@U.TIME.1@:@U.TIME.1@     AFTpunkt ;    !01:00 - 01:19
%01@U.TIME.2@:@U.TIME.2@     BEFpunkt ;    !01:20 - 01:59
%02@U.TIME.2@:@U.TIME.2@     AFTpunkt ;    !02:00 - 02:19
%02@U.TIME.3@:@U.TIME.3@     BEFpunkt ;    !02:20 - 02:59
%03@U.TIME.3@:@U.TIME.3@     AFTpunkt ;    !03:00 - 03:19
%03@U.TIME.4@:@U.TIME.4@     BEFpunkt ;    !03:20 - 03:59
%04@U.TIME.4@:@U.TIME.4@     AFTpunkt ;    !04:00 - 04:19
%04@U.TIME.5@:@U.TIME.5@     BEFpunkt ;    !04:20 - 04:59
%05@U.TIME.5@:@U.TIME.5@     AFTpunkt ;    !05:00 - 05:19
%05@U.TIME.6@:@U.TIME.6@     BEFpunkt ;    !05:20 - 05:59
%06@U.TIME.6@:@U.TIME.6@     AFTpunkt ;    !06:00 - 06:19
%06@U.TIME.7@:@U.TIME.7@     BEFpunkt ;    !06:20 - 06:59
%07@U.TIME.7@:@U.TIME.7@     AFTpunkt ;    !07:00 - 07:19
%07@U.TIME.8@:@U.TIME.8@     BEFpunkt ;    !07:20 - 07:59
%08@U.TIME.8@:@U.TIME.8@     AFTpunkt ;    !08:00 - 08:19
%08@U.TIME.9@:@U.TIME.9@     BEFpunkt ;    !08:20 - 08:59
%09@U.TIME.9@:@U.TIME.9@     AFTpunkt ;    !09:00 - 09:19
%09@U.TIME.10@:@U.TIME.10@   BEFpunkt ;    !09:20 - 09:59

1%0@U.TIME.10@:@U.TIME.10@   AFTpunkt ;     !10:00 - 10:19
1%0@U.TIME.11@:@U.TIME.11@   BEFpunkt ;     !10:20 - 10:59
11@U.TIME.11@:@U.TIME.11@    AFTpunkt ;     !11:00 - 11:59
11@U.TIME.12@:@U.TIME.12@    BEFpunkt ;     !11:19 - 11:59
12@U.TIME.12@:@U.TIME.12@    AFTpunkt ;     !12:00 - 12:19
12@U.TIME.1@:@U.TIME.1@      BEFpunkt ;     !12:19 - 12:59
13@U.TIME.1@:@U.TIME.1@      AFTpunkt ;     !13:00 - 13:19
13@U.TIME.2@:@U.TIME.2@      BEFpunkt ;     !13:19 - 13:59
14@U.TIME.2@:@U.TIME.2@      AFTpunkt ;     !14:00 - 114:19
14@U.TIME.3@:@U.TIME.3@      BEFpunkt ;     !14:19 - 14:59
15@U.TIME.3@:@U.TIME.3@      AFTpunkt ;     !15:00 - 15:19
15@U.TIME.4@:@U.TIME.4@      BEFpunkt ;     !15:19 - 15:59
16@U.TIME.4@:@U.TIME.4@      AFTpunkt ;     !16:00 - 16:19
16@U.TIME.5@:@U.TIME.5@      BEFpunkt ;     !16:19 - 16:59
17@U.TIME.5@:@U.TIME.5@      AFTpunkt ;     !17:00 - 17:19
17@U.TIME.6@:@U.TIME.6@      BEFpunkt ;     !17:19 - 17:59
18@U.TIME.6@:@U.TIME.6@      AFTpunkt ;     !18:00 - 18:19
18@U.TIME.7@:@U.TIME.7@      BEFpunkt ;     !18:19 - 18:59
19@U.TIME.7@:@U.TIME.7@      AFTpunkt ;     !19:00 - 19:19
19@U.TIME.8@:@U.TIME.8@      BEFpunkt ;     !19:20 - 19:59
2%0@U.TIME.8@:@U.TIME.8@     AFTpunkt ;     !20:00 - 20:19
2%0@U.TIME.9@:@U.TIME.9@     BEFpunkt ;     !20:20 - 20:59
21@U.TIME.9@:@U.TIME.9@      AFTpunkt ;     !21:00 - 21:19
21@U.TIME.10@:@U.TIME.10@    BEFpunkt ;     !21:20 - 21:59
22@U.TIME.10@:@U.TIME.10@    AFTpunkt ;     !22:00 - 22:19
22@U.TIME.11@:@U.TIME.11@    BEFpunkt ;     !22:19 - 22:59
23@U.TIME.11@:@U.TIME.11@    AFTpunkt ;     !23:00 - 23:19
23@U.TIME.12@:@U.TIME.12@    BEFpunkt ;     !23:19 - 23:59
%0%0@U.TIME.12@:@U.TIME.12@  AFTpunkt ;     !00:00 - 00:19
%0%0@U.TIME.1@:@U.TIME.1@    BEFpunkt ;     !00:19 - 00:59
24%:%0%0:kaksteist        #        ;     !24:00


LEXICON BEFpunkt
 %:+Use/NA: BEF ;
 %.+Use/NA: BEF ;

LEXICON AFTpunkt
 %:+Use/NA: AFT ;  ! 2:00 = kaks
 %.+Use/NA: AFT ;  ! 2.00 = kaks
 +Use/NA:  HOUR ;  ! 2    = kaks

LEXICON BEF
 2%0:kümne%               TOHALF ;
 21:üheksa%                TOHALF ;
 22:kaheksa%              TOHALF ;
 23:seitsme%              TOHALF ;
 24:kuue%                TOHALF ;
 25:viie%                TOHALF ;
 26:nelja%              TOHALF ;
 26+Use/NG:nelja%        TOHALF ;
 26+Use/NG:nelja%        TOHALF ;
 26+Use/NG:nelja%         TOHALF ;
 27:kolme%                 TOHALF ;
 28:kahe%                TOHALF ;
 29:ühe%                  TOHALF ;
 3%0:pool%               HOUR ;
 31:üks% minut%                  OVERHALF ;
 32:kaks% minutit%               OVERHALF ;
 33:kolm% minutit%                OVERHALF ;
 34:neli% minutit%             OVERHALF ;
 34+Use/NG:neli%        OVERHALF ;
 34+Use/NG:neli%        OVERHALF ;
 34+Use/NG:neli%         OVERHALF ;
 35:viis% minutit%               OVERHALF ;
 36:kuus% minutit%               OVERHALF ;
 37:seitse% minutit%             OVERHALF ;
 38:kaheksa% minutit%             OVERHALF ;
 39:üheksa% minutit%               OVERHALF ;
 4%0:kümme% minutit%              OVERHALF ;
 4%0:kahekümne%              TO ;
 41:üheksateistkümne%          TO ;
 42:kaheksateistkümne%        TO ;
 43:seitsmeteistkümne%        TO ;
 43:kahe%    TOQUARTER ;
 44:kuueteistkümne%          TO ;
 44:ühe%    TOQUARTER ;
 45:viieteistkümne%            TO ;
 45:kolmveerand%    HOUR ;
 46:neljateistkümne%        TO ;
 46:üks% minut% kolmveerand%    HOUR_OVER ;
! 46+Use/NG:luhkienjielje%  TO ;
! 46+Use/NG:luhkienieljie%  TO ;
! 46+Use/NG:luhkienielje%   TO ;
 47:kolmeteistkümne%           TO ;
 47:kaks% minutit% kolmveerand%     HOUR_OVER ;
 48:kaheteistkümne%          TO ;
 49:üheteistkümne%            TO ;
 5%0:kümne%               TO ;
 51:üheksa%                TO ;
 52:kaheksa%              TO ;
 53:seitsme%              TO ;
 54:kuue%                TO ;
 55:viie%                TO ;
 56:nelja%              TO ;
! 56+Use/NG:njielje%        TO ;
! 56+Use/NG:nieljie%        TO ;
! 56+Use/NG:nielje%         TO ;
 57:kolme%                 TO ;
 58:kahe%                TO ;
 59:ühe%                  TO ;

LEXICON AFT
 %0%0:                     HOUR ;
 %01:üks% minut%                 HOUR_OVER ;
 %02:kaks% minutit%               HOUR_OVER ;
 %03:kolm% minutit%                HOUR_OVER ;
 %04:neli% minutit%            HOUR_OVER ;
! %04+Use/NG:njielje%       HOUR_OVER ;
! %04+Use/NG:nieljie%       HOUR_OVER ;
! %04+Use/NG:nielje%        HOUR_OVER ;
 %05:viis% minutit%              HOUR_OVER ;
 %06:kuus% minutit%              HOUR_OVER ;
 %07:seitse% minutit%            HOUR_OVER ;
 %08:kaheksa% minutit%            HOUR_OVER ;
 %09:üheksa% minutit%              HOUR_OVER ;
 1%0:kümme% minutit%              HOUR_OVER ;
 11:üksteist% minutit%           HOUR_OVER ;
 12:kaksteist% minutit%         HOUR_OVER ;
 13:kolmteist% minutit%          HOUR_OVER ;
 13:kahe%    TO_QUARTER_OVER ;
 14:neliteist% minutit%       HOUR_OVER ;
 14:ühe%    TO_QUARTER_OVER ;
! 14+Use/NG:luhkienjielje%  HOUR_OVER ;
! 14+Use/NG:luhkienieljie%  HOUR_OVER ;
! 14+Use/NG:luhkienielje%   HOUR_OVER ;
 15:viisteist% minutit%           HOUR_OVER ;
 15:veerand%    HOUR_PLUS ;
 16:kuusteist% minutit%         HOUR_OVER ;
 16:üks% minut% veerand%    HOUR_PLUS ;
 17:seitseteist% minutit%       HOUR_OVER ;
 17:kaks% minutit% veerand%     HOUR_PLUS ;
 18:kaheksateist% minutit%       HOUR_OVER ;
 19:üheksateist% minutit%         HOUR_OVER ;
 20:kakskümmend% minutit%         HOUR_OVER ;

LEXICON TOHALF
 :minuti% pärast% pool%         HOUR ;

LEXICON OVERHALF
 :pool%        HOUR_OVER ;

LEXICON TO
        :minuti% pärast%          HOUR ;

LEXICON TOQUARTER
        :minuti% pärast% kolmveerand%          HOUR ;

LEXICON TO_QUARTER_OVER
        :minuti% pärast% veerand%          HOUR_PLUS_OVER ;


LEXICON HOUR
@U.TIME.1@:üks@U.TIME.1@           # ;
@U.TIME.2@:kaks@U.TIME.2@         # ;
@U.TIME.3@:kolm@U.TIME.3@          # ;
@U.TIME.4@:neli@U.TIME.4@       # ;
@U.TIME.4@+Use/NG:neli@U.TIME.4@ # ;
@U.TIME.4@+Use/NG:neli@U.TIME.4@ # ;
@U.TIME.4@+Use/NG:neli@U.TIME.4@  # ;
@U.TIME.5@:viis@U.TIME.5@         # ;
@U.TIME.6@:kuus@U.TIME.6@         # ;
@U.TIME.7@:seitse@U.TIME.7@       # ;
@U.TIME.8@:kaheksa@U.TIME.8@       # ;
@U.TIME.9@:üheksa@U.TIME.9@         # ;
@U.TIME.10@:kümme@U.TIME.10@       # ;
@U.TIME.11@:üksteist@U.TIME.11@   # ;
@U.TIME.12@:kaksteist@U.TIME.12@ # ;

LEXICON HOUR_PLUS
@U.TIME.1@:kaks@U.TIME.1@           # ;
@U.TIME.2@:kolm@U.TIME.2@         # ;
@U.TIME.3@:neli@U.TIME.3@          # ;
@U.TIME.4@:viis@U.TIME.4@       # ;
@U.TIME.4@+Use/NG:neli@U.TIME.4@ # ;
@U.TIME.4@+Use/NG:neli@U.TIME.4@ # ;
@U.TIME.4@+Use/NG:neli@U.TIME.4@  # ;
@U.TIME.5@:kuus@U.TIME.5@         # ;
@U.TIME.6@:seitse@U.TIME.6@         # ;
@U.TIME.7@:kaheksa@U.TIME.7@       # ;
@U.TIME.8@:üheksa@U.TIME.8@       # ;
@U.TIME.9@:kümme@U.TIME.9@         # ;
@U.TIME.10@:üksteist@U.TIME.10@       # ;
@U.TIME.11@:kaksteist@U.TIME.11@   # ;
@U.TIME.12@:üks@U.TIME.12@ # ;

LEXICON HOUR_PLUS_OVER
@U.TIME.1@:kaks% @U.TIME.1@           OVER ;
@U.TIME.2@:kolm% @U.TIME.2@         OVER ;
@U.TIME.3@:neli% @U.TIME.3@          OVER ;
@U.TIME.4@:viis% @U.TIME.4@       OVER ;
@U.TIME.5@:kuus% @U.TIME.5@         OVER ;
@U.TIME.6@:seitse% @U.TIME.6@         OVER ;
@U.TIME.7@:kaheksa% @U.TIME.7@       OVER ;
@U.TIME.8@:üheksa% @U.TIME.8@       OVER ;
@U.TIME.9@:kümme% @U.TIME.9@         OVER ;
@U.TIME.10@:üksteist% @U.TIME.10@       OVER ;
@U.TIME.11@:kaksteist% @U.TIME.11@   OVER ;
@U.TIME.12@:üks% @U.TIME.12@ OVER ;

LEXICON HOUR_OVER
@U.TIME.1@:üks% @U.TIME.1@           OVER ;
@U.TIME.2@:kaks% @U.TIME.2@         OVER ;
@U.TIME.3@:kolm% @U.TIME.3@          OVER ;
@U.TIME.4@:neli% @U.TIME.4@       OVER ;
@U.TIME.5@:viis% @U.TIME.5@         OVER ;
@U.TIME.6@:kuus% @U.TIME.6@         OVER ;
@U.TIME.7@:seitse% @U.TIME.7@       OVER ;
@U.TIME.8@:kaheksa% @U.TIME.8@       OVER ;
@U.TIME.9@:üheksa% @U.TIME.9@         OVER ;
@U.TIME.10@:kümme% @U.TIME.10@       OVER ;
@U.TIME.11@:üksteist% @U.TIME.11@   OVER ;
@U.TIME.12@:kaksteist% @U.TIME.12@ OVER ;

LEXICON OVER
    :läbi%   # ;


