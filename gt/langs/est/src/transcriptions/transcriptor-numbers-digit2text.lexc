! Divvun & Giellatekno - open source grammars for Sámi and other languages
! Copyright © 2000-2010 The University of Tromsø & the Norwegian Sámi Parliament
! http://giellatekno.uit.no & http://divvun.no
!
! This program is free software; you can redistribute and/or modify
! this file under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version. The GNU General Public License
! is found at http://www.gnu.org/licenses/gpl.html. It is
! also available in the file $GTHOME/LICENSE.txt.
!
! Other licensing options are available upon request, please contact
! giellatekno@hum.uit.no or divvun@samediggi.no

Multichar_Symbols

  +Use/NG    ! Do not generate, for isme-ped.fst and apertium
  +String    ! Tag to denote non-numeric strings
  +NumNum    ! Tag to denote real numbers in one form or another

LEXICON Root
< %+String [a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|q|y|z|õ|ä|ö|ü|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|Õ|Ä|Ö|Ü]* > COMMA ; ! This first line is to allow all letter strings.
+NumNum NUMBERSECTION ;
+NumNum COMMASECTION ;

LEXICON NUMBERSECTION
HUNDREDSM ;		! 200M
sada:1 HUNDREDM ;	! 100M
TENSM ;			! 20-99M
TEENSM ;		! 10-19M
ONESM ;			! 1-9M
HUNDREDST ;		! 200000-999999
sada:1 HUNDREDT ;	! 100000-100999
TENST ;			! 20000-99999,10000-10999
TEENST ;		! 11000-19999
ONEST ;			! 2000-9999
tuhat:1 THOUSAND ;	! 1000-1999
UNDERTHOUSAND ;		! 100-999
TENS ;			! 20-99
TEENS ;			! 10-19
ONES ;			! 1-9
ZERO ;          	! 0

LEXICON ZERO
null:0 #;

LEXICON HUNDREDSM
kaks:2  CUODIM ;
kolm:3 CUODIM ;
neli:4 CUODIM ; 
viis:5 CUODIM ;
kuus:6 CUODIM ;
seitse:7 CUODIM ;
kaheksa:8 CUODIM ;
üheksa:9 CUODIM ;

LEXICON CUODIM
sada: HUNDREDM ;

LEXICON HUNDREDM
! TENM ;
TENSM ;
TEENSM ;
:%0 ONESM ;
:%0%0 MILJON ;

LEXICON TEENSM
:1 TEENM ;

!LEXICON TEENSM
!lohkie:1 LOHKAIM ;

LEXICON TEENSM-OLD
  LOHKAIM ;
kümmend+Use/NG:  LOHKAIM ; !!= @CODE@ Analyzed but not generated

LEXICON TEENM
üks:1 LOHKAIM ;
kaks:2 LOHKAIM ;
kolm:3 LOHKAIM ;
neli:4 LOHKAIM ;
viis:5 LOHKAIM ;
kuus:6 LOHKAIM ;
seitse:7 LOHKAIM ;
kaheksa:8 LOHKAIM ;
üheksa:9 LOHKAIM ;

LEXICON LOHKAIM
teist: MILJON ;

LEXICON TENSM
kaks:2 LÅGEVM ;
kolm:3 LÅGEVM ;
neli:4 LÅGEVM ;
viis:5 LÅGEVM ;
kuus:6 LÅGEVM ;
seitse:7 LÅGEVM ;
kaheksa:8 LÅGEVM ;
üheksa:9 LÅGEVM ;

LEXICON LÅGEVM
# ;



LEXICON LUHKIEM
luhkie:%0 MILJON ;
luhkie:   ONESM ;

LEXICON ONESM
üks% miljon% :1 OVERTHOUSANDS ;
kaks:2         MILJON ;
kolm:3          MILJON ;
neli:4       MILJON ;
viis:5         MILJON ;
kuus:6         MILJON ;
seitse:7       MILJON ;
kaheksa:8       MILJON ;
üheksa:9         MILJON ; 

LEXICON MILJON
% miljonit : OVERTHOUSANDS ;

! =================
! Under the million
! =================

LEXICON OVERTHOUSANDS
HUNDREDST ;		! x00.000.000
sada:1 HUNDREDT ;	! 100.000.000
:%0 TENST ;		!  20.000.000
:%0 TEENST ;		!  10.000.000
:%0%0 ONEST ;		!   2.000.000
tuhat:%0%01 THOUSAND ;	!   1.000.000
:%0%0%0 UNDERTHOUSAND ;	
:%0%0%0%0 TENS ;
:%0%0%0%0 TEENS ;
:%0%0%0%0%0 ONES ;
:%0%0%0%0%0%0 COMMA ;	!  x000000

LEXICON HUNDREDST
kaks:2   CUODIT ;	! 200000-299999
kolm:3    CUODIT ;	! 300000-399999
neli:4 CUODIT ;	! 400000-499999
viis:5   CUODIT ;	! 500000-599999
kuus:6   CUODIT ;	! 600000-699999
seitse:7 CUODIT ;	! 700000-799999
kaheksa:8 CUODIT ;	! 800000-899999
üheksa:9   CUODIT ;	! 900000-999999

LEXICON CUODIT
sada: HUNDREDT ;	! 100000-199999

LEXICON HUNDREDT	! X = 1-9, Y = 0-9
TENST ;			! X2XYYY, X10YYY
TEENST ;		! X1XYYY
üks:%01 THOUSANDS ;
:%0 ONEST ;		! XX0YYY
:%0%0 THOUSANDS ;	! X00YYY

LEXICON TEENST
:1   TEENT ;

LEXICON TEENT
üks:1     LOHKAIT ;
kaks:2   LOHKAIT ;
kolm:3    LOHKAIT ;
neli:4 LOHKAIT ;
viis:5   LOHKAIT ;
kuus:6   LOHKAIT ;
seitse:7 LOHKAIT ;
kaheksa:8 LOHKAIT ;
üheksa:9   LOHKAIT ;

LEXICON LOHKAIT
teist: THOUSANDS ;


LEXICON TENST
kaks:2 LÅGEVT ;
kolm:3 LÅGEVT ;
neli:4 LÅGEVT ;
viis:5 LÅGEVT ;
kuus:6 LÅGEVT ;
seitse:7 LÅGEVT ;
kaheksa:8 LÅGEVT ;
üheksa:9 LÅGEVT ;

LEXICON LÅGEVT
#;

LEXICON TENT
kümme% tuhat:1%0 THOUSAND ;


LEXICON LUHKIET
:%0 THOUSANDS ;
akte:1 THOUSANDS ;
ONEST ;

LEXICON ONEST
kaks:2 THOUSANDS ;
kolm:3 THOUSANDS ;
neli:4 THOUSANDS ;
viis:5 THOUSANDS ;
kuus:6 THOUSANDS ;
seitse:7 THOUSANDS ;
kaheksa:8 THOUSANDS ;
üheksa:9 THOUSANDS ;

LEXICON THOUSANDS      ! x > 1
tuhat: THOUSAND ;

LEXICON THOUSAND
UNDERTHOUSAND ;	   ! 1100-1999
:%0 TENS ;	   ! 1020-1099
:%0 TEENS ;	   ! 1010-1019
:%0%0 ONES ;	   ! 1001-1009
:%0%0%0 COMMA ;	   ! 1000

!===========================
!Here starts the 999 numbers
!===========================

LEXICON UNDERTHOUSAND
HUNDREDS ;
sada:1 HUNDRED ;

LEXICON HUNDREDS
kaks:2   CUODI ;
kolm:3    CUODI ;
neli:4 CUODI ;
viis:5   CUODI ;
kuus:6   CUODI ;
seitse:7 CUODI ;
kaheksa:8 CUODI ;
üheksa:9   CUODI ;

LEXICON CUODI
sada:   HUNDRED ;

LEXICON HUNDRED
      TENS ;
      TEENS ;
:%0   ONES ;
:%0%0 COMMA ;

LEXICON CUODAAT
:%0%0%.    COMMA ;

LEXICON TEENS
:1 TEEN ;

LEXICON TEEN
üks:1 LOHKAI ;
kaks:2 LOHKAI ;
kolm:3 LOHKAI ;
neli:4 LOHKAI ;
viis:5 LOHKAI ;
kuus:6 LOHKAI ;
seitse:7 LOHKAI ;
kahdeksa:8 LOHKAI ;
üheksa:9 LOHKAI ;

LEXICON LOHKAI
teist: # ;


LEXICON TENS
luhkie:1%0 COMMA ;
låhkede:1%0%. COMMA ;
göökte:2 LUHKIE ;
golme:3 LUHKIE ;
njieljie:4 LUHKIE ;
njielje+Use/NG:4 LUHKIE ;
nieljie+Use/NG:4 LUHKIE ;
nielje+Use/NG:4 LUHKIE ;
vïjhte:5 LUHKIE ;
govhte:6 LUHKIE ;
tjïjhtje:7 LUHKIE ;
gaektsie:8 LUHKIE ;
uktsie:9 LUHKIE ;

LEXICON LUHKIE
luhkie:%0 COMMA ;
låhkede:%0%. COMMA ;
luhkie: ONES ;

LEXICON ONES
CARDINAL ;
ORDINAL ;

LEXICON CARDINAL
üks:1 COMMA ;
kaks:2 COMMA ;
kolm:3 COMMA ;
neli:4 COMMA ;
viis:5 COMMA ;
kuus:6 COMMA ;
seitse:7 COMMA ;
kaheksa:8 COMMA ;
üheksa:9 COMMA ;

LEXICON ORDINAL
esimene:1%.     COMMA ;
teine:2%.   COMMA ;
kolmas:3%.   COMMA ;
neljas:4%. COMMA ;
viies:5%.   COMMA ;
kuues:6%.   COMMA ;
seitmes:7%.    COMMA ;
kaheksas:8%.   COMMA ;
üheksas:9%.    COMMA ;

LEXICON COMMA
 ENDLEX ;
 COMMASECTION ;

LEXICON COMMASECTION  ! these are not spellchecked!!!
                  ENDLEX ;
! % komma% :,      Root ;
! % tjuohkkis% :%. Root ;
! % kolon% :%:     Root ;
! % sárggis% :%-   Root ; 
 % la% :%=         Root ; 
 % gráda% :%°      Root ; 
 % paragráfa% :§   Root ; 
! % násti% :%*     Root ; 
 % ja% :&          Root ; 

LEXICON ENDLEX
 # ;
