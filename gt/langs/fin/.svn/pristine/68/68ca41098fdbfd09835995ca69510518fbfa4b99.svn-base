!!!Other symbols
Punctuation characters detailed here are the characters that appear commonly
in Finnish texts, but are not part of words or linguistic content. The
punctuations control clause and sentence level annotations, and range
from full stops and commas to brackets. While punctuation symbols might
have limited use as inflecting units, the ones described here refer to
punctuation symbols as used in their primary purpose, in isolation. The
part of language norms controlling punctuation are from orthography and
the references of punctuation in good language use are not from the grammar
but issues of Kielikello journal on good Finnish language use. The most
current issue on punctuation was [Kielikello 
2/2006|http://arkisto.kielikello.fi/index.php?mid=2&pid=12&maid=110] (N.B.
you may need to buy subscription or route through university servers).


The primary punctuation marks are sentence final punctuation, they mark
the end of a sentence. The most typical of these is full stop symbol, which
ends neutral sentences. The exclamation mark and question mark end
exclamative and questioning sentences respectively. An elliptic or unfinished
sentence is ended with three successive full stops. In sloppy writing style
it is common to use two, four or more full stops to mark an elliptic
sentence. The Unicode compatibility character ellipsis has never been used
for Finnish language and must not be used. Same applies for other
combinations of sentence ending punctuation marks, the most common of these
have separate analyses.

__Punctuation and symbols examples:__
* __.__: {{.+Punct}} (Eng. full stop)

The clause level punctuation marks are used in clause boundaries. The most
typical of these is comma. The colon and semicolon are too. The clause
boundaries do not have separate semantics needed in applications so they only
have analyses for clause boundaries.


The brackets are used to offset portions of text in opening and closing
pairs. The most common pair is round brackets. Others used in Finnish are
square, curly and angle brackets, in somewhat decreasing order of commonness.
The angle brackets are commonly replaced by lower than symbol for opening and
greater than symbol for closing bracket.
The bracketed question mark is used to indicate uncertainty and bracketed
exclamation mark to indicate surprise, both of these annotations are used
within sentence as other bracketed constructions.

The quotation marks are used to offset quotations. The typical ones in
Finnish are the 9-shaped double quotation marks and apostrophes. Angle
quotation marks can also be used, primarily in books and newspapers.
It is possible to replace curly quotation marks with neutral typewriter
ones where technology limits. It is also common to see foreign quotation
marks or accent marks in place of quotation marks in sloppy writing style.

There are two different dashes in Finnish. The hyphen is used for mainly
word internally and won't appear as itself. The dash is used to offset
some sentences or mark elision. The dash symbol can be either of unicode
dash symbols or replaced with dash offset by spaces. In sloppy writing,
two hyphens are often used in place of dashes.

The space is used to separate words. For most applications the space has
separate meaning so it rarely gets used as a symbol in applications of

Less used symbols that appear in the Finnish texts; these do not have 
special analyses. A slash can be used as a replacement of the meaning 'or',
as a division slash or as a separator of verses in poem. 
Backslash is used only in computer systems.
Underscore is used only in computer systems.
The pipe is used in dictionaries as morhpeme boundary, and computer systems.
At sign is used only in computer systems.
An ampersand can be used as a replacement of the meaning 'and'.
Percent symbol is used after numeric expressions meaning 0.01 multiplier.
Permille symbol is used after numeric expressions meaning 0.001 multiplier.
§ sign is used for numbering sections etc.
Degree sign is used with measurements. The second and minute signs can be
used in conjunction with degree sign.
The plus sign, specific minus sign and plus-minus signs are used in
numeric expressions. The multiplication sign is used in numeric expressions.
The equals sign is used in formulae. The asterisk is used as a marker
for ungrammatical constructions and computer and other expressions.
Registered and trademark symbols are rarely used. Copyright symbol is
rarely used. Hash sign is used in phones and computer systems.
The doubled § sign has been used as chapter range sign. The pilcrow
sign can be used to mark chapters.
The currency signs for euro, dollar, pound sterling, cent and yen can be
used. I don't think anyone uses the currency sign ¤ ever.
The lines below this one are not from any referenced source
