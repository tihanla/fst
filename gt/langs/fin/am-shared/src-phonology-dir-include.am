## Process this file with automake to produce Makefile.in

## Copyright (C) 2011 Samediggi

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

####### Source file defs: ########

#! @param GT_PHONOLOGY_MAIN required, the source of phonology
#! @param GT_PHONOLOGY_SUPPLEMENTS optional, other sources to distribute and
#! 		  compile for other things
#! Both @params are specified in $(GTLANG)/src/phonology/Makefile.am.
GT_PHONOLOGY_SRCS=$(GT_PHONOLOGY_MAIN) $(GT_PHONOLOGY_SUPPLEMENTS)

# All sources need to be included in the tarball
EXTRA_DIST=$(GT_PHONOLOGY_SRCS)

####### Automake targets: ########

# The rule transducer will be built, not installed
GT_PHONOLOGY=
if CAN_HFST
GT_PHONOLOGY+=$(GTLANG)-phon.compose.hfst
GT_PHONOLOGY+=$(GTLANG)-phon.lookup.hfst
endif
if CAN_XFST
GT_PHONOLOGY+=$(GTLANG)-phon.compose.xfst
GT_PHONOLOGY+=$(GTLANG)-phon.lookup.xfst
endif

if WANT_L2
if CAN_HFST
GT_PHONOLOGY+=$(GTLANG)-phon-L2.compose.hfst
GT_PHONOLOGY+=$(GTLANG)-phon-L2.lookup.hfst
endif # CAN_HFST
if CAN_XFST
GT_PHONOLOGY+=$(GTLANG)-phon-L2.compose.xfst
GT_PHONOLOGY+=$(GTLANG)-phon-L2.lookup.xfst
endif # CAN_XFST
endif # WANT_L2

noinst_DATA=$(GT_PHONOLOGY)

####### Other targets: ###########
clean-local:
	-rm -f *.hfst *.xfst

##########################################
# General build rules included from here:#
include $(top_srcdir)/am-shared/twolc-include.am
include $(top_srcdir)/am-shared/xfscript-include.am
include $(top_srcdir)/am-shared/lookup-include.am
include $(top_srcdir)/am-shared/silent_build-include.am

# vim: set ft=automake:
