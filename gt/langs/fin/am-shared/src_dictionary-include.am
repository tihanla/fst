## Include this file in top-level srcdir to compile FSTs
## Copyright: Sámediggi/Divvun/UiT
## Licence: GPL v3+

####### Automake targets: ########

if WANT_DICTIONARIES
if CAN_HFST
GT_ANALYSERS_HFST+=analyser-dict-gt-desc.hfstol \
				   analyser-dict-gt-desc-mobile.hfstol
GT_GENERATORS_HFST+=generator-dict-gt-norm.hfstol
endif # CAN_HFST

if CAN_XFST
GT_ANALYSERS_XFST+=analyser-dict-gt-desc.xfst \
				   analyser-dict-gt-desc-mobile.xfst
GT_GENERATORS_XFST+=generator-dict-gt-norm.xfst
endif # CAN_XFST
endif # WANT_DICTIONARIES

##### END Xerox target list #####

# Building dictionary fsts. The dictionary analyser differs from the
# regular analyser by NOT removing variant and homonym tags, so that we can
# generate proper (mini)paradigms for the given lemmas and variants (the
# analysis string is used as input to the generator).
# Tags to be removed:
#   - Sem/-taggar
#   - OLang/*
#   - MWE
analyser-dict-gt-desc.tmp.hfst: generator-raw-gt-desc.hfst        \
					filters/remove-derivation-position-tags.hfst  \
					filters/remove-dialect-tags.hfst              \
					filters/remove-norm-comp-tags.hfst            \
					filters/remove-orig_lang-tags.hfst            \
					filters/remove-usage_except_NGminip-tags.hfst \
					filters/remove-semantic-tags.hfst             \
					filters/remove-hyphenation-marks.hfst         \
					filters/remove-infl_deriv-borders.hfst        \
					filters/remove-word-boundary.hfst             \
					filters/remove-orthography-tags.hfst          \
					filters/remove-Orth_IPA-strings.hfst          \
					orthography/inituppercase.compose.hfst        \
					orthography/spellrelax.compose.hfst           \
					orthography/downcase-derived_proper-strings.compose.hfst
	$(AM_V_HXFST)$(PRINTF) "set xerox-composition ON \n\
			read regex \
				@\"filters/remove-derivation-position-tags.hfst\"  \
			.o. @\"filters/remove-dialect-tags.hfst\"              \
			.o. @\"filters/remove-norm-comp-tags.hfst\"            \
			.o. @\"filters/remove-orig_lang-tags.hfst\"            \
			.o. @\"filters/remove-usage_except_NGminip-tags.hfst\" \
			.o. @\"filters/remove-semantic-tags.hfst\"             \
			.o. @\"filters/remove-orthography-tags.hfst\"          \
			.o. @\"filters/remove-Orth_IPA-strings.hfst\"          \
			.o. @\"$<\"                                            \
			.o. @\"orthography/downcase-derived_proper-strings.compose.hfst\" \
			.o. @\"filters/remove-hyphenation-marks.hfst\"         \
			.o. @\"filters/remove-infl_deriv-borders.hfst\"        \
			.o. @\"filters/remove-word-boundary.hfst\"             \
			; \n\
			define fst \n\
			set flag-is-epsilon ON\n\
			read regex fst \
			.o. @\"orthography/inituppercase.compose.hfst\"        \
			.o. @\"orthography/spellrelax.compose.hfst\"           \
			;\n\
		 save stack $@\n\
		 quit\n" | $(HFST_XFST) -p $(VERBOSITY)

analyser-dict-gt-desc.tmp.xfst: analyser-raw-gt-desc.xfst         \
					filters/remove-derivation-position-tags.xfst  \
					filters/remove-dialect-tags.xfst              \
					filters/remove-norm-comp-tags.xfst            \
					filters/remove-orig_lang-tags.xfst            \
					filters/remove-usage_except_NGminip-tags.xfst \
					filters/remove-semantic-tags.xfst             \
					filters/remove-hyphenation-marks.xfst         \
					filters/remove-infl_deriv-borders.xfst        \
					filters/remove-word-boundary.xfst             \
					filters/remove-orthography-tags.xfst          \
					filters/remove-Orth_IPA-strings.xfst          \
					orthography/inituppercase.compose.xfst        \
					orthography/spellrelax.compose.xfst           \
					orthography/downcase-derived_proper-strings.compose.xfst
	$(AM_V_XFST)$(PRINTF) "read regex \
				@\"filters/remove-derivation-position-tags.xfst\"  \
			.o. @\"filters/remove-dialect-tags.xfst\"              \
			.o. @\"filters/remove-norm-comp-tags.xfst\"            \
			.o. @\"filters/remove-orig_lang-tags.xfst\"            \
			.o. @\"filters/remove-usage_except_NGminip-tags.xfst\" \
			.o. @\"filters/remove-semantic-tags.xfst\"             \
			.o. @\"filters/remove-orthography-tags.xfst\"          \
			.o. @\"filters/remove-Orth_IPA-strings.xfst\"          \
			.o. @\"$<\"                                            \
			.o. @\"orthography/downcase-derived_proper-strings.compose.xfst\" \
			.o. @\"filters/remove-hyphenation-marks.xfst\"         \
			.o. @\"filters/remove-infl_deriv-borders.xfst\"        \
			.o. @\"filters/remove-word-boundary.xfst\"             \
			; \n\
			define fst \n\
			set flag-is-epsilon ON\n\
			read regex fst \
			.o. @\"orthography/inituppercase.compose.xfst\"        \
			.o. @\"orthography/spellrelax.compose.xfst\"           \
			;\n\
		 save stack $@\n\
		 quit\n" | $(XFST) $(VERBOSITY)

# The mobile analyser is just like the regular dictionary analyser,
# except for one additional set of spellrelaxes:
analyser-dict-gt-desc-mobile.tmp.hfst: analyser-dict-gt-desc.tmp.hfst \
					orthography/spellrelax-mobile-keyboard.compose.hfst
	$(AM_V_RGX2FST)$(PRINTF)                                    "\
			    @\"$<\"                                          \
			.o. @\"orthography/spellrelax-mobile-keyboard.compose.hfst\" \
			;" \
		| $(HFST_REGEXP2FST) $(VERBOSITY) $(HFST_FLAGS) -S --xerox-composition=ON \
		> $@

analyser-dict-gt-desc-mobile.tmp.xfst: analyser-dict-gt-desc.tmp.xfst \
					orthography/spellrelax-mobile-keyboard.compose.xfst
	$(AM_V_XFST)$(PRINTF)                            "read regex \
			    @\"$<\"                                          \
			.o. @\"orthography/spellrelax-mobile-keyboard.compose.xfst\" \
			;\n\
		 save stack $@\n\
		 quit\n" | $(XFST) $(VERBOSITY)

# The generator does NOT NGminip tagged strings, since we do
# not want them in the miniparadigms generated by this transducer. At the same
# time, this transducer DOES CONTAIN the homonym and variant tags, to govern
# proper paradigm generation.
# Obligatory tags:
#   - homonymy tags
#   - variant tags
#   - Use/NGminip ??? - check with Lene!
generator-dict-gt-norm.tmp.hfst: generator-raw-gt-desc.hfst       \
					filters/make-optional-transitivity-tags.hfst  \
					filters/make-optional-semantic-tags.hfst      \
					filters/remove-derivation-position-tags.hfst  \
					filters/remove-dialect-tags.hfst              \
					filters/remove-norm-comp-tags.hfst            \
					filters/remove-orig_lang-tags.hfst            \
					filters/remove-usage_except_NGminip-tags.hfst \
					filters/remove-orthography-tags.hfst          \
					filters/remove-Orth_IPA-strings.hfst          \
					filters/remove-sub-forms.hfst                 \
					filters/remove-hyphenation-marks.hfst         \
					filters/remove-infl_deriv-borders.hfst        \
					filters/remove-word-boundary.hfst             \
					orthography/downcase-derived_proper-strings.compose.hfst
	$(AM_V_HXFST)$(PRINTF) "set xerox-composition ON \n\
			read regex \
				@\"filters/make-optional-transitivity-tags.hfst\"  \
			.o. @\"filters/make-optional-semantic-tags.hfst\"      \
			.o. @\"filters/remove-derivation-position-tags.hfst\"  \
			.o. @\"filters/remove-dialect-tags.hfst\"              \
			.o. @\"filters/remove-norm-comp-tags.hfst\"            \
			.o. @\"filters/remove-orig_lang-tags.hfst\"            \
			.o. @\"filters/remove-usage_except_NGminip-tags.hfst\" \
			.o. @\"filters/remove-sub-forms.hfst\"                 \
			.o. @\"filters/remove-orthography-tags.hfst\"          \
			.o. @\"filters/remove-Orth_IPA-strings.hfst\"          \
			.o. @\"$<\"                                            \
			.o. @\"orthography/downcase-derived_proper-strings.compose.hfst\" \
			.o. @\"filters/remove-hyphenation-marks.hfst\"         \
			.o. @\"filters/remove-infl_deriv-borders.hfst\"        \
			.o. @\"filters/remove-word-boundary.hfst\"             \
			;\n\
		 save stack $@\n\
		 quit\n" | $(HFST_XFST) -p $(VERBOSITY)

generator-dict-gt-norm.tmp.xfst: analyser-raw-gt-desc.xfst        \
					filters/make-optional-transitivity-tags.xfst  \
					filters/make-optional-semantic-tags.xfst      \
					filters/remove-derivation-position-tags.xfst  \
					filters/remove-dialect-tags.xfst              \
					filters/remove-norm-comp-tags.xfst            \
					filters/remove-orig_lang-tags.xfst            \
					filters/remove-usage_except_NGminip-tags.xfst \
					filters/remove-orthography-tags.xfst          \
					filters/remove-Orth_IPA-strings.xfst          \
					filters/remove-sub-forms.xfst                 \
					filters/remove-hyphenation-marks.xfst         \
					filters/remove-infl_deriv-borders.xfst        \
					filters/remove-word-boundary.xfst             \
					orthography/downcase-derived_proper-strings.compose.xfst
	$(AM_V_XFST)$(PRINTF)                             "read regex  \
				@\"filters/make-optional-transitivity-tags.xfst\"  \
			.o. @\"filters/make-optional-semantic-tags.xfst\"      \
			.o. @\"filters/remove-derivation-position-tags.xfst\"  \
			.o. @\"filters/remove-dialect-tags.xfst\"              \
			.o. @\"filters/remove-norm-comp-tags.xfst\"            \
			.o. @\"filters/remove-orig_lang-tags.xfst\"            \
			.o. @\"filters/remove-usage_except_NGminip-tags.xfst\" \
			.o. @\"filters/remove-sub-forms.xfst\"                 \
			.o. @\"filters/remove-orthography-tags.xfst\"          \
			.o. @\"filters/remove-Orth_IPA-strings.xfst\"          \
			.o. @\"$<\"                                            \
			.o. @\"orthography/downcase-derived_proper-strings.compose.xfst\" \
			.o. @\"filters/remove-hyphenation-marks.xfst\"         \
			.o. @\"filters/remove-infl_deriv-borders.xfst\"        \
			.o. @\"filters/remove-word-boundary.xfst\"             \
			;\n\
		 invert net\n\
		 save stack $@\n\
		 quit\n" | $(XFST) $(VERBOSITY)
