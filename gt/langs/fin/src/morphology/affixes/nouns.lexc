!! !!!Noun inflection and derivation
!! Nouns and other nominals inflect in number, cases, possessives and with
!! clitics, in that order. Combinations that this regular inflection can form
!! is approximately 2×15×5×26=4900, so we do not show all variants in test cases
!! and examples, but just central ones that are interesting and potential to
!! break.

!! !!Nominatives
!! Singular nominative is the dictionary reference form for most of the words.

LEXICON N_BACK_NOMINATIVE
!!€gt-norm: Noun nominative back
!!€ talo talo+N+Sg+Nom a house
!!€ taloko talo+N+Sg+Nom+Qst
!!€ talohan talo+N+Sg+Nom+Foc/han
!!$ talohän talo+N+Sg+Nom+Foc/han
+Sg+Nom:0   N_BACK_CLIT_OPT  ;

LEXICON N_FRONT_NOMINATIVE
!!€gt-norm: Noun nominative front
!!€ pöly pöly+N+Sg+Nom dust
+Sg+Nom:0   N_FRONT_CLIT_OPT  ;

!! The plural nominative attaches to singular stem. For plural words it is also
!! the form that is used for dictionary lookups:

LEXICON N_BACK_PLNOM_CLIT
!!€gt-norm: Noun nominative plural back
!!€ talot talo+N+Pl+Nom 
!!€ sakset sakset+N+Pl+Nom scissors
!!€ saksetpa sakset+N+Pl+Nom+Foc/pa
!!$ saksetpä sakset+N+Pl+Nom+Foc/pa
+Pl+Nom:%>t     N_BACK_CLIT_OPT  ;

LEXICON N_FRONT_PLNOM_CLIT
!!€gt-norm: Noun nominative plural front
!!€ pölyt pöly+N+Pl+Nom
!!€ häät häät+N+Pl+Nom  wedding
+Pl+Nom:%>t     N_FRONT_CLIT_OPT  ;

!! !!Singular case inflection
!! The nouns can inflect in 15 regular cases. Most of the cases have one or two
!! case endings, with only varying part being the harmony vowel. For nouns with
!! direct consonant gradation, the most of the singular case suffixes attach
!! to the weak singular stem:

LEXICON N_BACK_WEAK_SINGULARS
!!€gt-norm: Noun basic singular back
!!€ pata pata+N+Sg+Nom pot
!!€ padan pata+N+Sg+Gen
!!€ padassa pata+N+Sg+Ine
!!€ padasta pata+N+Sg+Ela
!!€ padalla pata+N+Sg+Ade
!!€ padalta pata+N+Sg+Abl
!!€ padalle pata+N+Sg+All
!!€ padaksi pata+N+Sg+Tra
!!€ padatta pata+N+Sg+Abe
0    N_BACK_PLNOM_CLIT   ;
+Sg+Gen:%>n     N_BACK_CLIT_OPT   ;
+Sg+Abe:%>tta   N_BACK_POSS_AN_OPT      ;
+Sg+Abl:%>lta   N_BACK_POSS_AN_OPT      ;
+Sg+Ade:%>lla   N_BACK_POSS_AN_OPT      ;
+Sg+All:%>lle   N_BACK_POSS_EN_OPT      ;
+Sg+Ela:%>sta   N_BACK_POSS_AN_OPT      ;
+Sg+Ine:%>ssa   N_BACK_POSS_AN_OPT      ;
+Sg+Tra:%>kse   N_BACK_POSS_EN_OBL       ;
+Sg+Tra:%>ksi   N_BACK_CLIT_OPT     ;
+Sg+Tra:%>ksi   N_BACK_CLIT_OPT     ;

LEXICON N_FRONT_WEAK_SINGULARS
!!€gt-norm: Noun basic singular front
!!€ näky näky+N+Sg+Nom vision
!!€ näyn näky+N+Sg+Gen
!!€ näyssä näky+N+Sg+Ine
!!€ näystä näky+N+Sg+Ela
!!€ näyllä näky+N+Sg+Ade
!!€ näyltä näky+N+Sg+Abl
!!€ näylle näky+N+Sg+All
!!€ näyksi näky+N+Sg+Tra
!!€ näyttä näky+N+Sg+Abe
0    N_FRONT_PLNOM_CLIT   ;
+Sg+Gen:%>n     N_FRONT_CLIT_OPT   ;
+Sg+Abe:%>ttä   N_FRONT_POSS_ÄN_OPT      ;
+Sg+Abl:%>ltä   N_FRONT_POSS_ÄN_OPT      ;
+Sg+Ade:%>llä   N_FRONT_POSS_ÄN_OPT      ;
+Sg+All:%>lle   N_FRONT_POSS_EN_OPT      ;
+Sg+Ela:%>stä   N_FRONT_POSS_ÄN_OPT      ;
+Sg+Ine:%>ssä   N_FRONT_POSS_ÄN_OPT      ;
+Sg+Tra:%>kse   N_FRONT_POSS_EN_OBL       ;
+Sg+Tra:%>ksi   N_FRONT_CLIT_OPT     ;
+Sg+Tra:%>ksi   N_FRONT_CLIT_OPT     ;

!! For words with direct gradation the only forms that attach to strong singular
!! stem may be essive and possessive's of nominative or genitive:

LEXICON N_BACK_STRONG_SINGULARS
!!€gt-norm: Noun basic singular strong back
!!€ patana pata+N+Sg+Ess
!!€ patani pata+N+Sg+Nom+PxSg1
!!€ patasi pata+N+Sg+Gen+PxSg2
+Sg+Ess:%>na N_BACK_POSS_AN_OPT      ;
+Sg+Nom:0  N_BACK_POSS   ;
+Sg+Gen:0  N_BACK_POSS   ;
+Pl+Nom:0  N_BACK_POSS   ;

LEXICON N_FRONT_STRONG_SINGULARS
!!€gt-norm: Noun basic singular strong front
!!€ näkynä näky+N+Sg+Ess
!!€ näkyni näky+N+Sg+Nom+PxSg1
!!€ näkysi näky+N+Sg+Gen+PxSg2
+Sg+Ess:%>nä N_FRONT_POSS_ÄN_OPT      ;
+Sg+Nom:0  N_FRONT_POSS   ;
+Sg+Gen:0  N_FRONT_POSS   ;
+Pl+Nom:0  N_FRONT_POSS   ;

!! !!Plural inflection

LEXICON N_BACK_WEAK_PLURALS
!!€gt-norm: Noun basic plural back
!!€ padoilta pata+N+Pl+Abl
!!€ padoilla pata+N+Pl+Ade
!!€ padoille pata+N+Pl+All
!!€ padoista pata+N+Pl+Ela
!!€ padoissa pata+N+Pl+Ine
!!€ padoiksi pata+N+Pl+Tra
!!€ padoitta pata+N+Pl+Abe
!!€ padoin pata+N+Pl+Ins
+Pl+Abe:%>i%>tta   N_BACK_POSS_AN_OPT      ;
+Pl+Abl:%>i%>lta   N_BACK_POSS_AN_OPT      ;
+Pl+Ade:%>i%>lla   N_BACK_POSS_AN_OPT      ;
+Pl+All:%>i%>lle  N_BACK_POSS_AN_OPT      ;
+Pl+Ela:%>i%>sta   N_BACK_POSS_AN_OPT      ;
+Pl+Ine:%>i%>ssa   N_BACK_POSS_AN_OPT      ;
+Pl+Ins:%>i%>n    N_BACK_CLIT_OPT     ;
+Pl+Tra:%>i%>kse  N_BACK_POSS_EN_OBL        ;
+Pl+Tra:%>i%>ksi  N_BACK_CLIT_OPT     ;

LEXICON N_FRONT_WEAK_PLURALS
!!€gt-norm: Noun basic plural front
!!€ näyillä näky+N+Pl+Ade
!!€ näyiltä näky+N+Pl+Abl
!!€ näyille näky+N+Pl+All
!!€ näyissä näky+N+Pl+Ine
!!€ näyistä näky+N+Pl+Ela
!!€ näyiksi näky+N+Pl+Tra
!!€ näyittä näky+N+Pl+Abe
!!€ näyin näky+N+Pl+Ins
+Pl+Abe:%>i%>ttä   N_FRONT_POSS_ÄN_OPT      ;
+Pl+Abl:%>i%>ltä   N_FRONT_POSS_ÄN_OPT      ;
+Pl+Ade:%>i%>llä   N_FRONT_POSS_ÄN_OPT      ;
+Pl+All:%>i%>lle  N_FRONT_POSS_ÄN_OPT      ;
+Pl+Ela:%>i%>stä   N_FRONT_POSS_ÄN_OPT      ;
+Pl+Ine:%>i%>ssä   N_FRONT_POSS_ÄN_OPT      ;
+Pl+Ins:%>i%>n    N_FRONT_CLIT_OPT     ;
+Pl+Tra:%>i%>kse  N_FRONT_POSS_EN_OBL        ;
+Pl+Tra:%>i%>ksi  N_FRONT_CLIT_OPT     ;

!! The strong plural stem of words with direct gradation contains only essive
!! and comitative:

LEXICON N_BACK_STRONG_PLURALS
!!€gt-norm: Noun basic plural back strong
!!€ patoina pata+N+Pl+Ess
!!€ patoine pata+N+Com
+Pl+Ess:%>i%>na   N_BACK_POSS_AN_OPT      ;
+Com:%>i%>ne        N_BACK_POSS_EN_OPT      ;

LEXICON N_FRONT_STRONG_PLURALS
!!€gt-norm: Noun basic plural front strong
!!€ näkyinä näky+N+Pl+Ess
!!€ näkyine näky+N+Com
+Pl+Ess:%>i%>nä   N_FRONT_POSS_ÄN_OPT      ;
+Com:%>i%>ne        N_FRONT_POSS_EN_OPT      ;

!! !!Cases with allomorphic variation
!! The nominal cases which can take several different suffixes are singular
!! and plural partitives, singular and plural illatives and plural genitives.
!! After stem variation the selection of these allomorphs is the main factor
!! of morphological classification of nouns.

!! The reconstructed historical suffix for partitives of Finnish is ða, ðä, the
!! current variants according to that theory would be the different
!! realisations of extinct ð. 

!! For basic vowel stems the partitive suffix is a, ä.
LEXICON N_BACK_PARTITIVE_A
!!€gt-norm: Noun singular partitive a
!!€ politiikka politiikka+N+Sg+Nom politics
!!€ politiikkaa politiikka+N+Sg+Par
+Sg+Par:%>a      N_BACK_POSS_OPT      ;

LEXICON N_FRONT_PARTITIVE_Ä
!!€gt-norm: Noun singular partitive ä
!!€ kysyntä kysyntä+N+Sg+Nom demand
!!€ kysyntää kysyntä+N+Sg+Par
+Sg+Par:%>ä      N_FRONT_POSS_OPT      ;

!! In stems other than -a, -ä stems, the 3rd possessive suffix may appear in
!! -an, -än form after the partitive suffix.
LEXICON N_BACK_PARTITIVE_A_AN
!!€gt-norm: Noun singular partitive a poss aan
!!€ pato pato+N+Sg+Nom dam
!!€ patoa pato+N+Sg+Par
!!€ patoaan pato+N+Sg+Par+PxSg3
+Sg+Par:%>a      N_BACK_POSS_AN_OPT      ;

LEXICON N_FRONT_PARTITIVE_Ä_ÄN
!!€gt-norm: Noun singular partitive ä poss ään
!!€ näkyä näky+N+Sg+Par
!!€ näkyään näky+N+Sg+Par+PxSg3
0+Sg+Par:%>ä     N_FRONT_POSS_ÄN_OPT     ;

!! The consonant stems and long vowel stems regularly take -ta, -tä suffix 
!! for singular partitives. It is up to interpretation whether the partitive
!! suffix of the -e^ stem is considered to be -tta, -ttä, or just -ta, -tä.
!! In principle the consonant that disappeared from -e^ stems could be from
!! that.

LEXICON N_BACK_PARTITIVE_TA
!!€gt-norm: Noun singular partitive ta
!!€ taimen taimen+N+Sg+Nom trout
!!€ taimenta taimen+N+Sg+Par
+Sg+Par:%>ta     N_BACK_POSS_AN_OPT      ;

LEXICON N_FRONT_PARTITIVE_TÄ
!!€gt-norm: Noun singular partitive tä
!!€ tie tie+N+Sg+Nom road
!!€ tietä tie+N+Sg+Par
!!€ vene vene+N+Sg+Nom boat
!!€ venettä vene+N+Sg+Par
+Sg+Par:%>tä     N_FRONT_POSS_ÄN_OPT      ;

!! The singular illatives have more variants.

!! The variant with intervening h attaches to long vowel stems:
LEXICON N_BACK_ILLATIVE_HAN
!!€gt-norm: Noun singular illative han
!!€ maa maa+N+Sg+Nom earth
!!€ maahan maa+N+Sg+Ill
!!€ maahansa maa+N+Sg+Ill+PxSg3
+Sg+Ill:%>han    N_BACK_CLIT_OPT         ;
+Sg+Ill:%>ha     N_BACK_POSS     ;

LEXICON N_BACK_ILLATIVE_HEN
!!€gt-norm: Noun singular illative hen back
!!€ toffee toffee+N+Sg+Nom taffy
!!€ toffeehen toffee+N+Sg+Ill
+Sg+Ill:%>hen    N_BACK_CLIT_OPT         ;
+Sg+Ill:%>he     N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_HEN
!!€gt-norm: Noun singular illative hen front
!!€ tiehen tie+N+Sg+Ill
+Sg+Ill:%>hen    N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>he     N_FRONT_POSS     ;

LEXICON N_BACK_ILLATIVE_HIN
!!€gt-norm: Noun singular illative hin back
!!€ hai hai+N+Sg+Nom shark
!!€ haihin hai+N+Sg+Ill
+Sg+Ill:%>hin    N_BACK_CLIT_OPT         ;
+Sg+Ill:%>hi     N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_HIN
!!€gt-norm: Noun singular illative hin front
!!€ pii pii+N+Sg+Nom silica
!!€ piihin pii+N+Sg+Ill
+Sg+Ill:%>hin    N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>hi     N_FRONT_POSS     ;

LEXICON N_BACK_ILLATIVE_HON
!!€gt-norm: Noun singular illative hon
!!€ suo suo+N+Sg+Nom swamp
!!€ suohon suo+N+Sg+Ill
+Sg+Ill:%>hon    N_BACK_CLIT_OPT         ;
+Sg+Ill:%>ho     N_BACK_POSS     ;

LEXICON N_BACK_ILLATIVE_HUN
!!€gt-norm: Noun singular illative hun
!!€ puu puu+N+Sg+Nom tree
!!€ puuhun puu+N+Sg+Ill
+Sg+Ill:%>hun    N_BACK_CLIT_OPT         ;
+Sg+Ill:%>hu     N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_HYN
!!€gt-norm: Noun singular illative hyn
!!€ pyy pyy+N+Sg+Nom kind of a bird
!!€ pyyhyn pyy+N+Sg+Ill
+Sg+Ill:%>hyn    N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>hy     N_FRONT_POSS     ;

LEXICON N_FRONT_ILLATIVE_HÄN
!!€gt-norm: Noun singular illative hän
!!€ pää pää+N+Sg+Nom head
!!€ päähän pää+N+Sg+Ill
+Sg+Ill:%>hän    N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>hä     N_FRONT_POSS     ;

LEXICON N_FRONT_ILLATIVE_HÖN
!!€gt-norm: Noun singular illative hän
!!€ työ työ+N+Sg+Nom job
!!€ työhön työ+N+Sg+Ill
+Sg+Ill:%>hön    N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>hö     N_FRONT_POSS     ;

!! The stems with short vowel do not have the intervening h.

LEXICON N_BACK_ILLATIVE_AN
!!€gt-norm: Noun singular illative an
!!€ kirja kirja+N+Sg+Nom book
!!€ kirjaan kirja+N+Sg+Ill
+Sg+Ill:%>an     N_BACK_CLIT_OPT         ;
+Sg+Ill:%>a      N_BACK_POSS     ;

LEXICON N_BACK_ILLATIVE_EN
!!€gt-norm: Noun singular illative en ba<ck
!!€ tuli tuli+N+Sg+Nom fire
!!€ tuleen tuli+N+Sg+Ill
+Sg+Ill:%>en     N_BACK_CLIT_OPT         ;
+Sg+Ill:%>e      N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_EN
!!€gt-norm: Noun singular illative en front
!!€ hiiri hiiri+N+Sg+Nom mouse
!!€ hiireen hiiri+N+Sg+Ill
+Sg+Ill:%>en     N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>e      N_FRONT_POSS     ;

LEXICON N_BACK_ILLATIVE_IN
!!€gt-norm: Noun singular illative in back
!!€ ruuvi ruuvi+N+Sg+Nom screw
!!€ ruuviin ruuvi+N+Sg+Ill
+Sg+Ill:%>in     N_BACK_CLIT_OPT         ;
+Sg+Ill:%>i      N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_IN
!!€gt-norm: Noun singular illative in front
!!€ tyyli tyyli+N+Sg+Nom style
!!€ tyyliin tyyli+N+Sg+Ill
+Sg+Ill:%>in     N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>i      N_FRONT_POSS     ;

LEXICON N_BACK_ILLATIVE_ON
!!€gt-norm: Noun singular illative on
!!€ taloon talo+N+Sg+Ill
+Sg+Ill:%>on     N_BACK_CLIT_OPT         ;
+Sg+Ill:%>o      N_BACK_POSS     ;

LEXICON N_BACK_ILLATIVE_UN
!!€gt-norm: Noun singular illative un
!!€ nielu nielu+N+Sg+Nom throat
!!€ nieluun nielu+N+Sg+Ill
+Sg+Ill:%>un     N_BACK_CLIT_OPT         ;
+Sg+Ill:%>u      N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_YN
!!€gt-norm: Noun singular illative yn
!!€ näkyyn näky+N+Sg+Ill
+Sg+Ill:%>yn     N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>y      N_FRONT_POSS     ;

LEXICON N_FRONT_ILLATIVE_ÄN
!!€gt-norm: Noun singular illative än
!!€ räkä räkä+N+Sg+Nom booger
!!€ räkään räkä+N+Sg+Ill
+Sg+Ill:%>än     N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>ä      N_FRONT_POSS     ;

LEXICON N_FRONT_ILLATIVE_ÖN
!!€gt-norm: Noun singular illative ön
!!€ tönö tönö+N+Sg+Nom shack
!!€ tönöön tönö+N+Sg+Ill
+Sg+Ill:%>ön     N_FRONT_CLIT_OPT         ;
+Sg+Ill:%>ö      N_FRONT_POSS     ;

!! Bisyllabic long vowel stems have illative suffix of -seen.
LEXICON N_BACK_ILLATIVE_SEEN
!!€gt-norm: Noun singular illative seen back
!!€ tienoo tienoo+N+Sg+Nom neighbourhood
!!€ tienooseen tienoo+N+Sg+Ill
+Sg+Ill:%>seen      N_BACK_CLIT_OPT     ;
+Sg+Ill:%>see       N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_SEEN
!!€gt-norm: Noun singular illative seen front
!!€ miljöö miljöö+N+Sg+Nom millieu
!!€ miljööseen miljöö+N+Sg+Ill
+Sg+Ill:%>seen      N_FRONT_CLIT_OPT     ;
+Sg+Ill:%>see       N_FRONT_POSS     ;

!! Plural genitives have the most variants. Especially, many words have more or
!! less free variation between handful of choices.
LEXICON N_BACK_GENITIVE_IDEN
!!€gt-norm:     Noun plural genitive iden back
!!€ aavikko     aavikko+N+Sg+Nom
!!€ aavikoiden  aavikko+N+Pl+Gen
+Pl+Gen:%>i%>den    N_BACK_CLIT_OPT     ;
+Pl+Gen:%>i%>de     N_BACK_POSS     ;

LEXICON N_FRONT_GENITIVE_IDEN
!!€gt-norm: Noun plural genitive iden front
!!€ kännykkä kännykkä+N+Sg+Nom mobile phone
!!€ kännyköiden kännykkä+N+Pl+Gen
+Pl+Gen:%>i%>den    N_FRONT_CLIT_OPT     ;
+Pl+Gen:%>i%>de     N_FRONT_POSS     ;

LEXICON N_BACK_GENITIVE_ITTEN
!!€gt-norm: Noun plural genitive itten back
!!€ tienoitten tienoo+N+Pl+Gen
+Pl+Gen:%>i%>tten     N_BACK_CLIT_OPT     ;
+Pl+Gen:%>i%>tte      N_BACK_POSS     ;

LEXICON N_FRONT_GENITIVE_ITTEN
!!€gt-norm: Noun plural genitive itten front
!!€ teitten tie+N+Pl+Gen
+Pl+Gen:%>i%>tten     N_FRONT_CLIT_OPT     ;
+Pl+Gen:%>i%>tte      N_FRONT_POSS     ;

LEXICON N_BACK_GENITIVE_IEN
!!€gt-norm: Noun plural genitive ien back
!!€ nainen nainen+N+Sg+Nom woman
!!€ naisien nainen+N+Pl+Gen
+Pl+Gen:%>i%>en   N_BACK_CLIT_OPT     ;
+Pl+Gen:%>i%>e    N_BACK_POSS     ;

LEXICON N_FRONT_GENITIVE_IEN
!!€gt-norm: Noun plural genitive ien back
!!€ murha murha+N+Sg+Nom murder
!!€ murhien murha+N+Pl+Gen
+Pl+Gen:%>i%>en   N_FRONT_CLIT_OPT     ;
+Pl+Gen:%>i%>e    N_FRONT_POSS     ;

LEXICON N_BACK_GENITIVE_JEN
!!€gt-norm: Noun plural genitive jen back
!!€ alivalikkojen alivalikko+N+Pl+Gen
+Pl+Gen:%>j%>en    N_BACK_CLIT_OPT     ;
+Pl+Gen:%>j%>e     N_BACK_POSS     ;

LEXICON N_FRONT_GENITIVE_JEN
!!€gt-norm: Noun plural genitive jen front
!!€ pyhäkkö pyhäkkö+N+Sg+Nom sanctum
!!€ pyhäkköjen pyhäkkö+N+Pl+Gen
+Pl+Gen:%>j%>en    N_FRONT_CLIT_OPT     ;
+Pl+Gen:%>j%>e     N_FRONT_POSS     ;

LEXICON N_BACK_GENITIVE_TEN
!!€gt-norm: Noun plural genitive ten back
!!€ sisar sisar+N+Sg+Nom sister
!!€ sisarten sisar+N+Pl+Gen
+Pl+Gen:%>ten    N_BACK_CLIT_OPT     ;
+Pl+Gen:%>te     N_BACK_POSS     ;

LEXICON N_FRONT_GENITIVE_TEN
!!€gt-norm: Noun plural genitive ten front
!!€ väännin väännin+N+Sg+Nom turner
!!€ väänninten väännin+N+Pl+Gen
+Pl+Gen:%>ten    N_FRONT_CLIT_OPT     ;
+Pl+Gen:%>te     N_FRONT_POSS     ;

!! The -in suffix for plural genitive that goes with singular stem is always
!! markedly archaic. Most commonly it appears in compound words.

LEXICON N_BACK_GENITIVE_IN
!!€gt-desc: Noun plural genitive rare in back
!!€ patain pata+N+Pl+Gen+Use/Rare
+Pl+Gen+Use/Rare:%>i%>n    N_BACK_CLIT_OPT     ;
+Pl+Gen+Use/Rare:%>i     N_BACK_POSS     ;

LEXICON N_FRONT_GENITIVE_IN
!!€gt-desc: Noun plural genitive rare in front
!!€ tyrmä tyrmä+N+Sg+Nom prison
!!€ tyrmäin tyrmä+N+Pl+Gen+Use/Rare
+Pl+Gen+Use/Rare:%>i%>n    N_FRONT_CLIT_OPT     ;
+Pl+Gen+Use/Rare:%>i     N_FRONT_POSS     ;

!! Plural partitive has a few variants:

LEXICON N_BACK_PARTITIVE_IA
!!€gt-norm: Noun plural partitive ia
!!€ koiria koira+N+Pl+Par
+Pl+Par:%>i%>a    N_BACK_POSS_AN_OPT      ;

LEXICON N_FRONT_PARTITIVE_IÄ
!!€gt-norm: Noun plural partitive iä
!!€ kärsä kärsä+N+Sg+Nom trunk
!!€ kärsiä kärsä+N+Pl+Par
+Pl+Par:%>i%>ä    N_FRONT_POSS_ÄN_OPT      ;

LEXICON N_BACK_PARTITIVE_ITA
!!€gt-norm: Noun plural partitive ita
!!€ tienoita tienoo+N+Pl+Par
+Pl+Par:%>i%>ta   N_BACK_POSS_AN_OPT      ;

LEXICON N_FRONT_PARTITIVE_ITÄ
!!€gt-norm: Noun plural partitive itä
!!€ teitä tie+N+Pl+Par
+Pl+Par:%>i%>tä   N_FRONT_POSS_ÄN_OPT      ;

LEXICON N_BACK_PARTITIVE_JA
!!€gt-norm: Noun plural partitive ja
!!€ lepakkoja lepakko+N+Pl+Par
+Pl+Par:%>j%>a     N_BACK_POSS_AN_OPT      ;

LEXICON N_FRONT_PARTITIVE_JÄ
!!€gt-norm: Noun plural partitive jä
!!€ pyhäkköjä pyhäkkö+N+Pl+Par
+Pl+Par:%>j%>ä     N_FRONT_POSS_ÄN_OPT      ;

!! And plural illative has few variants:

LEXICON N_BACK_ILLATIVE_IHIN
!!€gt-norm: Noun plural illative ihin back
!!€ taloihin talo+N+Pl+Ill
+Pl+Ill:%>i%>hin   #    ;
+Pl+Ill:%>i%>hi    N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_IHIN
!!€gt-norm: Noun plural illative ihin front
!!€ näkyihin näky+N+Pl+Ill
+Pl+Ill:%>i%>hin   #    ;
+Pl+Ill:%>i%>hi    N_FRONT_POSS     ;

LEXICON N_BACK_ILLATIVE_IIN
!!€gt-norm: Noun plural illative iin back
!!€ koiriin koira+N+Pl+Ill
+Pl+Ill:%>i%>in    #     ;
+Pl+Ill:%>i%>i     N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_IIN
!!€gt-norm: Noun plural illative iin front
!!€ sieni sieni+N+Sg+Nom mushroom
!!€ sieniin sieni+N+Pl+Ill
+Pl+Ill:%>i%>in    #     ;
+Pl+Ill:%>i%>i     N_FRONT_POSS     ;

LEXICON N_BACK_ILLATIVE_ISIIN
!!€gt-norm: Noun plural illative isiin back
!!€ sokea sokea+N+Sg+Nom
!!€ sokeisiin sokea+N+Pl+Ill
+Pl+Ill:%>i%>siin  #     ;
+Pl+Ill:%>i%>sii   N_BACK_POSS     ;

LEXICON N_FRONT_ILLATIVE_ISIIN
!!€gt-norm: Noun plural illative isiin front
!!€ kevät kevät+N+Sg+Nom spring
!!€ keväisiin kevät+N+Pl+Ill
+Pl+Ill:%>i%>siin  #     ;
+Pl+Ill:%>i%>sii   N_FRONT_POSS     ;

!! !!Possessive suffixes
!! Possessives come optionally after the case suffixes. For consonant final
!! cases the possessives assimilate or eat the final part of the case ending
!! or stem.
LEXICON N_BACK_POSS
!!€gt-norm: Noun possessive back
!!€ taloni talo+N+Sg+Nom+PxSg1
!!€ talosi talo+N+Sg+Nom+PxSg2
!!€ talonsa talo+N+Sg+Nom+PxSg3
!!€ talomme talo+N+Sg+Nom+PxPl1
!!€ talonne talo+N+Sg+Nom+PxPl2
+PxSg1:%>ni  N_BACK_CLIT_OPT        ;
+PxSg2:%>si  N_BACK_CLIT_OPT        ;
+PxSg3:%>nsa N_BACK_CLIT_OPT        ;
+PxPl1:%>mme N_BACK_CLIT_OPT        ;
+PxPl2:%>nne N_BACK_CLIT_OPT        ;
+PxPl3:%>nsa N_BACK_CLIT_OPT        ;

LEXICON N_FRONT_POSS
!!€gt-norm: Noun possessive front
!!€ tönöni tönö+N+Sg+Nom+PxSg1
!!€ tönösi tönö+N+Sg+Nom+PxSg2
!!€ tönönsä tönö+N+Sg+Nom+PxSg3
!!€ tönömme tönö+N+Sg+Nom+PxPl1
!!€ tönönne tönö+N+Sg+Nom+PxPl2
+PxSg1:%>ni  N_FRONT_CLIT_OPT        ;
+PxSg2:%>si  N_FRONT_CLIT_OPT        ;
+PxSg3:%>nsä N_FRONT_CLIT_OPT        ;
+PxPl1:%>mme N_FRONT_CLIT_OPT        ;
+PxPl2:%>nne N_FRONT_CLIT_OPT        ;
+PxPl3:%>nsä N_FRONT_CLIT_OPT        ;

!! The possessive suffix of form -an, -än, attaches to some long vowel 
!! stems:

LEXICON N_BACK_POSS_AN
!!€gt-norm: Noun possessive an
!!€ taloaan talo+N+Sg+Par+PxPl3
+PxSg3:%>an    N_BACK_CLIT_OPT     ;
+PxPl3:%>an    N_BACK_CLIT_OPT     ;

LEXICON N_BACK_POSS_EN
!!€gt-norm: Noun possessive back en
!!€ tienookseen tienoo+N+Sg+Tra+PxSg3
+PxSg3:%>en    N_BACK_CLIT_OPT     ;
+PxPl3:%>en    N_BACK_CLIT_OPT     ;

LEXICON N_FRONT_POSS_EN
!!€gt-norm: Noun possessive front en
!!€ tiekseen tie+N+Sg+Tra+PxSg3
+PxSg3:%>en    N_FRONT_CLIT_OPT     ;
+PxPl3:%>en    N_FRONT_CLIT_OPT     ;

LEXICON N_FRONT_POSS_ÄN
!!€gt-norm: Noun possessive än
!!€ tönöään tönö+N+Sg+Par+PxPl3
+PxSg3:%>än    N_FRONT_CLIT_OPT     ;
+PxPl3:%>än    N_FRONT_CLIT_OPT     ;

!! !!Noun clitics
!! Clitics can attach to any word-form, including one that already has a clitic.
!! Clitics do not modify the form they attach to and are simply concatenated to
!! the end.

LEXICON N_BACK_CLIT
!!€gt-norm: Noun clitic back
!!€ talokaan talo+N+Sg+Nom+Foc/kaan
+Foc/han:%>han   #        ;
+Foc/han+Foc/kaan:%>han%>kaan  #        ;
+Foc/han+Qst:%>han%>ko #        ;
+Foc/han+Foc/pa:%>han%>pa #        ;
+Qst:%>ko #        ;
+Qst+Foc/han:%>ko%>han #        ;
+Qst+Foc/kaan:%>ko%>kaan    #        ;
+Qst+Foc/kin:%>ko%>kin    #        ;
+Qst+Foc/pa:%>ko%>pa   #        ;
+Qst+Foc/s:%>ko%>s    #        ;
+Foc/pa:%>pa #        ;
+Foc/pa+Foc/han:%>pa%>han #        ;
+Foc/pa+Foc/kaan:%>pa%>kaan    #        ;
+Foc/pa+Foc/kin:%>pa%>kin    #        ;
+Foc/pa+Qst:%>pa%>ko   #        ;
+Foc/pa+Foc/s:%>pa%>s    #        ;
+Foc/kin:%>kin  #        ;
+Foc/kin+Foc/han:%>kin%>han  #        ;
+Foc/kin+Foc/kaan:%>kin%>kaan #        ;
+Foc/kin+Qst:%>kin%>ko    #        ;
+Foc/kin+Foc/pa:%>kin%>pa    #        ;
+Foc/kaan:%>kaan  #        ;
+Foc/kaan+Foc/han:%>kaan%>han  #        ;
+Foc/kaan+Foc/kin:%>kaan%>kin #        ;
+Foc/kaan+Qst:%>kaan%>ko    #        ;
+Foc/kaan+Foc/pa:%>kaan%>pa    #        ;

LEXICON N_FRONT_CLIT
!!€gt-norm: Noun clitic front
!!€ tönökään tönö+N+Sg+Nom+Foc/kaan
+Foc/han:%>hän   #        ;
+Foc/han+Foc/kaan:%>hän%>kään  #        ;
+Foc/han+Qst:%>hän%>kö #        ;
+Foc/han+Foc/pa:%>hän%>pä #        ;
+Qst:%>kö #        ;
+Qst+Foc/han:%>kö%>hän #        ;
+Qst+Foc/kaan:%>kö%>kään    #        ;
+Qst+Foc/kin:%>kö%>kin    #        ;
+Qst+Foc/pa:%>kö%>pä   #        ;
+Qst+Foc/s:%>kö%>s    #        ;
+Foc/pa:%>pä #        ;
+Foc/pa+Foc/han:%>pä%>hän #        ;
+Foc/pa+Foc/kaan:%>pä%>kään    #        ;
+Foc/pa+Foc/kin:%>pä%>kin    #        ;
+Foc/pa+Qst:%>pä%>kö   #        ;
+Foc/pa+Foc/s:%>pä%>s    #        ;
+Foc/kin:%>kin  #        ;
+Foc/kin+Foc/han:%>kin%>hän  #        ;
+Foc/kin+Foc/kaan:%>kin%>kään #        ;
+Foc/kin+Qst:%>kin%>kö    #        ;
+Foc/kin+Foc/pa:%>kin%>pä    #        ;
+Foc/kaan:%>kään  #        ;
+Foc/kaan+Foc/han:%>kään%>hän  #        ;
+Foc/kaan+Foc/kin:%>kään%>kin #        ;
+Foc/kaan+Qst:%>kään%>kö    #        ;
+Foc/kaan+Foc/pa:%>kään%>pä    #        ;


! shorthands (could have tests as well...)

LEXICON N_BACK_CLIT_OPT
0    #   ;
0   N_BACK_CLIT    ;

LEXICON N_FRONT_CLIT_OPT
0    #   ;
0   N_FRONT_CLIT    ;

LEXICON N_BACK_SINGULARS
0    N_BACK_STRONG_SINGULARS ;
0    N_BACK_WEAK_SINGULARS   ;

LEXICON N_FRONT_SINGULARS
0    N_FRONT_STRONG_SINGULARS ;
0    N_FRONT_WEAK_SINGULARS   ;

LEXICON N_FRONT_PLURALS
0   N_FRONT_STRONG_PLURALS ;
0   N_FRONT_WEAK_PLURALS ;

LEXICON N_BACK_PLURALS
0   N_BACK_STRONG_PLURALS ;
0   N_BACK_WEAK_PLURALS ;

LEXICON N_BACK_POSS_AN_OPT
0    N_BACK_POSS   ;
0    N_BACK_POSS_AN ;
0    N_BACK_CLIT ;
0    #    ;

LEXICON N_FRONT_POSS_ÄN_OPT
0    N_FRONT_POSS   ;
0    N_FRONT_POSS_ÄN  ;
0    N_FRONT_CLIT ;
0    #    ;

LEXICON N_BACK_POSS_EN_OPT
0    N_BACK_POSS   ;
0    N_BACK_POSS_EN ;
0    N_BACK_CLIT ;
0    #    ;

LEXICON N_FRONT_POSS_EN_OPT
0    N_FRONT_POSS   ;
0    N_FRONT_POSS_EN  ;
0    N_FRONT_CLIT ;
0    #    ;

LEXICON N_BACK_POSS_OPT
0    N_BACK_POSS   ;
0    N_BACK_CLIT ;
0    #    ;

LEXICON N_FRONT_POSS_OPT
0    N_FRONT_POSS   ;
0    N_FRONT_CLIT ;
0    #    ;

LEXICON N_BACK_POSS_EN_OBL
0   N_BACK_POSS  ;
0   N_BACK_POSS_EN   ;

LEXICON N_FRONT_POSS_EN_OBL
0   N_FRONT_POSS  ;
0   N_FRONT_POSS_EN   ;

! derivs
!LEXICON N_BACK_DERIV_INEN
!i   N_AAKKOSELLINEN ;
!lai N_AAKKOSELLINEN ;
!
!LEXICON N_FRONT_DERIV_INEN
!i   N_KYLKIÄINEN ;
!läi N_KYLKIÄINEN ;
!
!LEXICON N_BACK_DERIV_TONTSE
!tse             ko-kaan+Adverb  ;
!lli             nen-se-soun+ta-en-ia-ien-ten-iin        ;
!llisuu          s-te-de-ks-toun+ta-en-ia-ien-iin        ;
!sti             ko-kaan+Adverb     ;
!t               ar-tare-taroun+ta-en-ia-ien-ten-iin   ;
!t               on-toma-tomoun+ta-an-ia-ien-(ten)-iin ;
!ttomuu          s-te-de-ks-toun+ta-en-ia-ien-iin     ;
!
!LEXICON N_FRONT_DERIV_TÖNTSE
!tse             ko-kaan+Adverb  ;
!lli             nen-se-soun+ta-en-ia-ien-ten-iin        ;
!llisuu          s-te-de-ks-toun+ta-en-ia-ien-iin        ;
!sti             ko-kaan+Adverb     ;
!t               ar-tare-taroun+ta-en-ia-ien-ten-iin   ;
!t               on-toma-tomoun+ta-an-ia-ien-(ten)-iin ;
!ttomuu          s-te-de-ks-toun+ta-en-ia-ien-iin     ;
!
!LEXICON N_BACK_DERIV_ITTAIN
!itse    ko-kaan+Adverb     ;
!ittain  ko-kaan+Adverb     ;
!!
!LEXICON N_FRONT_DERIV_ITTÄIN
!itse    kö-kään+Adverb     ;
!ittäin  kö-kään+Adverb     ;
!
!! !!Noun compounding
!! Nouns form compounds productively. The non-final parts of regular compounds
!! are singular nominatives, or singular or plural genitives of nouns. The final
!! parts are nominals and inflect regularly.

LEXICON N_COMPOUND_0
!!€gt-norm:
!!€ talojuttu talo+N+Sg+Nom#juttu+N+Sg+Nom
+Sg+Nom:0   N_COMPOUND  ;

LEXICON N_COMPOUND_N
!!€gt-norm:
!!€ talonjuttu talo+N+Sg+Gen#juttu+N+Sg+Nom
+Sg+Gen:%>n               N_COMPOUND    ;

LEXICON N_COMPOUND_IEN
!!€gt-norm:
!!€ naisienjuttu nainen+N+Pl+Gen#juttu+N+Sg+Nom
+Pl+Gen:%>i%>en                 N_COMPOUND  ;

LEXICON N_COMPOUND_IDEN
!!€gt-norm: 
!!€ lepakoidenjuttu lepakko+N+Pl+Gen#juttu+N+Sg+Nom
+Pl+Gen:%>i%>den                 N_COMPOUND  ;

LEXICON N_COMPOUND_IN
!!€gt-norm:
!!€ vanhempainjuttu vanhempi+N+Pl+Gen#juttu+N+Sg+Nom
+Pl+Gen:%>i%>n                 N_COMPOUND     ;

LEXICON N_COMPOUND_ITTEN
!!€gt-norm:
!!€ lepakoittenjuttu lepakko+N+Pl+Gen#juttu+N+Sg+Nom
+Pl+Gen:%>i%>tten               N_COMPOUND     ;

LEXICON N_COMPOUND_JEN
!!€gt-norm:
!!€ talojenjuttu talo+N+Pl+Gen#juttu+N+Sg+Nom
+Pl+Gen:%>j%>en                 N_COMPOUND     ;

LEXICON N_COMPOUND_TEN
!!€gt-norm:
!!€ miestenjuttu mies+N+Pl+Gen#juttu+N+Sg+Nom
+Pl+Gen:%>ten                 N_COMPOUND     ;


LEXICON N_COMPOUND_S
!!€gt-norm:
!!€ aakkostamisjuttu aakkostaminen+N+Der/s#juttu+N+Sg+Nom
+Der/s:»s                   N_COMPOUND  ;

LEXICON N_COMPOUND
!!€gt-norm:
!!€ talojuttu talo+N+Sg+Nom#juttu+N+Sg+Nom
+Use/Circ#:0#     NOMINAL   ;

!vim: set ft=xfst-lexc:
