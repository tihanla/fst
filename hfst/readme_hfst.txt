HFST morphologies are downloaded from:
https://sourceforge.net/projects/hfst/files/resources/morphological-transducers/

:Currently we use the precompiled hfstol binaries:
1 Download:
de: https://sourceforge.net/projects/hfst/files/resources/morphological-transducers/hfst-german-installable.tar.gz/download
fi: https://sourceforge.net/projects/hfst/files/resources/morphological-transducers/hfst-finnish-installable.tar.gz/download
fr: https://sourceforge.net/projects/hfst/files/resources/morphological-transducers/hfst-french-installable.tar.gz/download
it: https://sourceforge.net/projects/hfst/files/resources/morphological-transducers/hfst-italian-installable.tar.gz/download
sv: https://sourceforge.net/projects/hfst/files/resources/morphological-transducers/hfst-swedish-installable.tar.gz/download

2. extract the zip files:
gunzip hfst-german-installable.tar.gz
gunzip fst-finnish-installable.tar.gz
gunzip hfst-french-installable.tar.gz
gunzip hfst-italian-installable.tar.gz
gunzip hfst-swedish-installable.tar.gz


3.copy the binaries using our naming convetion:
cp hfst-german-installable/de-analysis.hfst.ol  work/de.ana.hfstol
cp hfst-finnish-installable/fi-analysis.hfst.ol work/fi.ana.hfstol
cp hfst-french-installable/fr-analysis.hfst.ol  work/fr.ana.hfstol
cp hfst-italian-installable/it-analysis.hfst.ol work/it.ana.hfstol
cp hfst-swedish-installable/sv-analysis.hfst.ol work/sv.ana.hfstol

cp hfst-german-installable/de-generation.hfst.ol  work/de.gen.hfstol
cp hfst-finnish-installable/fi-generation.hfst.ol work/fi.gen.hfstol
cp hfst-french-installable/fr-generation.hfst.ol  work/fr.gen.hfstol
cp hfst-italian-installable/it-generation.hfst.ol work/it.gen.hfstol
cp hfst-swedish-installable/sv-generation.hfst.ol work/sv.gen.hfstol



We also can modify and generate the files from the sources for this:

de:
download: 
https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/morphisto/morphisto.zip
unzip  into morphisto folder
follow the instructions in morphisto/README how to generate the SFST binary from the sources, which can be 
converted to HFST (see EMOR English morphology)
website of Morphisto: http://code.google.com/p/morphisto
The SFST homepage offers only the compiled SFST version for dowload:
http://www.cis.uni-muenchen.de/~schmid/tools/SMOR/

fi:
form https://github.com/flammie/omorfi  download the zipped repo as omorfi-master.zip
follow instructions in morfi-master/INSTALL  to generate the generate the HFST binary dictionaries.2

fr:
We have open linguistic resources of the French morphology, but we do not have the tool to generate the HFST!
But the source which was used to generate the shared in binary format can be downloaded from:
http://www.cnrtl.fr/lexiques/morphalou/licence_morphalou.php?version=2

it:
We have open linguistic resources of the Italian morphology, but we do not have the tool to generate the HFST!
But the source which was used to generate the shared in binary format can be downloaded from:
http://dev.sslmit.unibo.it/linguistics/downloads/morph-it.tgz
more info website (together with a precompiled binary SFST version):
http://dev.sslmit.unibo.it/linguistics/morph-it.php

sv:
download
https://sourceforge.net/projects/hfst/files/resources/morphological-transducers/hfst-swedish.tar.gz/download
unzip the  hfst-swedish.tar.gz file
Generate the HFST file with hfst-swedish/src/Makefile

