Installation of the SFST packages on Linux

SFST compiler (Debian packages):
1. download the appropriate version that match your Ubuntu/Debian from here:
http://packages.debian.org/sid/sfst
2. install the package e.g.: 
sudo dpkg -i sfst_1.4.6h-1_amd64.deb
the binaris (starting with fst-*) will be installed into /usr/bin


Apertium lex-tools (SFST to HFSTOL  conversion)
1. wget http://apertium.projectjj.com/apt/install-nightly.sh
2. install-nightly.sh
3. sudo apt-get install apertium-all-dev
the binaries (starting with hfst-*) will be installed to /usr/bin

EMOR SFST morphology:
The XTag source of the English morphology can be downloaded from: http://www.cis.uni-muenchen.de/~schmid/tools/SFST/data/EMOR.tar.gz
The name of the file is xtag-morph-1.5a.txt.
However we use the UTF-8 converted version of the file which is added to this repository for manual updates and extensions: xtag-morph.txt
Dowloading and merging with this file with original regularly can be a good idea...

Running the database generation:
make_emor.sh
The script first converts the XTag format source file into SFST language source file. Then this file is compilted into 
an SFST transducer which is then converted into HFT optimalized format. 

Results:
Generated in work subfoder:
en.ana.hfstol:  English analyser
en.gen.hfstol:  English generator









