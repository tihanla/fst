#This is a dictionary compiler of SFST EMOR English mororphology 
#from the XTag source it generates UTF-8 HFST transducers for morph analysis and generation
#more info: laszlo.tihanyi@ext.ec.europa.eu

set -x

#version=ansi
version=utf8

if [ $version == "ansi" ]; then
    wdir=./work_ansi
else
    wdir=./work
fi


mkdir -p $wdir

#We maintain the UTF-8 version of the XTag morphological dictionary: xtag-morph.txt
#For this step should be run only once at the first time to avoid overwriting manual work:
#if [ $version == "ansi" ]; then
#ANSI:
#cp xtag-morph-1.5a.txt  $wdir/xtag-morph.txt
#UTF-8
#else:
#iconv -f 8859_1 -t UTF-8 xtag-morph-1.5a.txt  > $wdir/xtag-morph.txt
#fi



perl ./perl/filter.perl < xtag-morph.txt  > $wdir/xtag-morph.filt.txt

if [ $version == "ansi" ]; then
echo match
#ANSI
perl ./perl/morph-match.perl $wdir/xtag-morph.filt.txt > $wdir/lexicon
else
#UTF8
perl ./perl/morph-match.perl -u $wdir/xtag-morph.filt.txt > $wdir/lexicon
fi

perl ./perl/make-morph.perl $wdir/lexicon > $wdir/emor.fst
perl ./perl/make-morph_utf8.perl $wdir/lexicon > $wdir/emor.fst


orig_path="$(pwd)"

cd $wdir

if [ $version == "ansi" ]; then
#ANSI:
fst-compiler emor.fst emor.fstdb
else
#UTF8:
fst-compiler-utf8 emor.fst emor.fstdb
fi
cd $orig_path
#

#TEST SFST analyser:
#fst-mor $wdir/emor.fstdb <testenana.txt  >$wdir/testenana_fst.txt

#SFST to HFSTOL:
hfst-fst2fst -t $wdir/emor.fstdb >$wdir/en.gen.hfst
#test gen hfst:
#hfst-lookup  $wdir/en.gen.hfst <testenana.txt >$wdir/testengen_lookup.hfst.txt

hfst-invert -i $wdir/en.gen.hfst -o $wdir/en.ana.hfst

#test ana hfst:
#hfst-lookup  $wdir/en.ana.hfst <testenana.txt  /$wdir/testenana_lookup.hfst.txt

hfst-fst2fst -O $wdir/en.ana.hfst >$wdir/en.ana.hfstol

#test ana hfstol
#hfst-lookup  $wdir/en.ana.hfstol <testenana.txt >$wdir/testenana_lookup.hfstol.txt

hfst-fst2fst -O $wdir/en.gen.hfst >$wdir/en.gen.hfstol
#test gen hfstol
#hfst-lookup  $wdir/en.gen.hfstol <testengen.txt >$wdir/testengen_lookup_hfstol.txt

#TEST hfstol with hfst-proc:
#hfst-proc $wdir/en.ana.hfstol testenana.txt  >$wdir/testenana_proc.out
#hfst-proc $wdir/en.gen.hfstol testengen.txt  >$wdir/testengen_proc.out







