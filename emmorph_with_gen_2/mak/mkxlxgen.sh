#!/bin/bash
export X=genX
export BASELEX=
export S=genX
export LEX=genX.lx2

export hpldir=..
source "$hpldir/mak/setroot.sh"
export EXCL="\"-excl=infsfx.*(?:IN|EX|AD)L|badorth\"" 
export SRFONLY="-generator -usesmcat"

logfile="$humlogdir""makeXLXgen$GUESS$x$s.log"
cd $hpldir/gen
make -rR -f $hpldir/mak/xlxu.make 2> "$logfile" $1 

source $hpldir/mak/errchk.sh

