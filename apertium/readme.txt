#Generation of optimized HSFST automatas from sources downloaded from Apertium website:

Apertium lex-tools (SFST to HFSTOL  conversion)
1. wget http://apertium.projectjj.com/apt/install-nightly.sh
2. install-nightly.sh
3. sudo apt-get install ml-tools
http://wiki.apertium.org/wiki/Prerequisites_for_Debian


Apertium Languge resources (16):
bg, cs, da, el,es, ga, hr, mt, nl, lt,lv,pl,pt,ro, sk,sl

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-bul
ape_make.sh apertium-bul\apertium-bul.bul.dix bg

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-ces
ape_make.sh apertium-ces\apertium-ces.ces.dix cs

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-dan
ape_make.sh apertium-dan\apertium-dan.dan.dix da

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-ell
ape_make.sh apertium-ell\apertium-ell.ell.dix  el

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-spa
ape_make.sh apertium-spa/apertium-en-es.en-es.dix es

svn checkout https://svn.code.sf.net/p/apertium/svn/incubator/apertium-gle
ape_make.sh apertium-gle\apertium-gle.gle.dix ga

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-hbs
ape_make.sh apertium-hbs\.deps\apertium-hbs.hbs_HR.dix hr

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-mlt
ape_make.sh apertium-mlt\apertium-mlt.mlt.dix mt

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-nld
ape_make.sh apertium-nld\apertium-nld.nld.dix nl

svn checkout https://svn.code.sf.net/p/apertium/svn/incubator/apertium-lit
ape_make.sh apertium-lit\apertium-lit.lit.dix  lt

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-lvs
ape_make.sh apertium-lvs\apertium-lvs.lvs.dix lv

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-pol
ape_make.sh apertium-pol\apertium-pol.pol.dix pl

#svn checkout  ??? 
ape_make.sh apertium-por\apertium-por.por.dix pt

#svn checkout ???
ape_make.sh apertium-ron\apertium-ron.ron.dix ro

svn checkout https://svn.code.sf.net/p/apertium/svn/incubator/apertium-slk
ape_make.sh apertium-slk\apertium-slk.slk.dix  sk

svn checkout https://svn.code.sf.net/p/apertium/svn/languages/apertium-slv
ape_make.sh apertium-slv\apertium-slv.slv.dix sl














