#dix=apertium-en-es.en-es.dix
#lng=es
dix=$1
lng=$2
lt-comp lr $dix $lng.automorf.bin
lt-print $lng.automorf.bin | sed 's/ /@_SPACE_@/g' | sed 's/ε/@0@/g' > $lng.automorf.att
hfst-txt2fst $lng.automorf.att -o $lng.automorf.hfst
hfst-fst2fst --optimized-lookup-weighted $lng.automorf.hfst -o work/$lng.automorf.hfst.ol
hfst-invert $lng.automorf.hfst -o $lng.automorf.gen.hfst
hfst-fst2fst --optimized-lookup-weighted $lng.automorf.gen.hfst -o work/$lng.automorf.gen.hfst.ol
