### FST Morphologies

This repository contains a collection of open source morphological resources.
The resources are available both in binary and source format.
Binaries can be used directly by different language applications. Sources together with the compilers  
can be used for improvments. 
The common binary format is HFSTOL (Helsinki FST optimzed lookup format).
Find the descriptions of each language resources in the following README files:

* bg: apertium/readme.txt
* cs: apertium/readme.txt
* da: apertium/readme.txt
* de: hfst/readme_hfst.txt
* el: apertium/readme.txt
* en: sfst/readme.txt
* es: apertium/readme.txt
* et: fst/gt/langs/est/README
* fi: hfst/readme_hfst.txt
* fr: hfst/readme_hfst.txt
* ga: apertium/readme.txt
* hr: apertium/readme.txt
* hu: hunmorph-foma/readme.txt
* it: hfst/readme_hfst.txt
* lt: apertium/readme.txt
* lv: apertium/readme.txt
* mt: apertium/readme.txt
* nl: apertium/readme.txt
* pl: apertium/readme.txt
* pt: apertium/readme.txt
* ro: apertium/readme.txt
* sl: apertium/readme.txt
* sk: apertium/readme.txt
* sv: hfst/readme_hfst.txt

Laszlo Tihanyi: tihanyi1123@gmail.com
EC DGT, Luxembourg